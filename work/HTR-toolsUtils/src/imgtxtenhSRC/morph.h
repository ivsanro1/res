/*
    Morphological functions for images
   
    Copyright (C) 2004-2013 Mauricio Villegas <mauvilsa@upv.es>

    This file is part of imgtxtenh.

    imgtxtenh is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MV_MORPH_H__
#define __MV_MORPH_H__

#include "imgio.h"

typedef struct {
  int x;
  int y;
} IV2;

typedef struct {
  int size;
  int max;
  IV2 *p;
} SE;

int seCircle(double radi,int centY,int centX,int origin,SE** _se);
int seRect(int seH,int seW,int centY,int centX,int origin,SE** _se);
int morphdilate_graym(gray** img, int imgW, int imgH, SE* se, gray** out);
int morpherode_graym(gray** img, int imgW, int imgH, SE* se, gray** out);
int morphopen_graym(gray** img, int imgW, int imgH, SE* se, gray** tmp, gray** out);
int morphclose_graym(gray** img, int imgW, int imgH, SE* se, gray** tmp, gray** out);

#endif
