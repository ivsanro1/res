/*
    Morphological functions for images
   
    Copyright (C) 2004-2013 Mauricio Villegas <mauvilsa@upv.es>

    This file is part of imgtxtenh.

    imgtxtenh is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "morph.h"

#include <stdlib.h>
#include <math.h>

int semalloc(int size,SE** _se) {
  *_se=(SE*)malloc(sizeof(SE)+size*sizeof(IV2));
  if(*_se==NULL)
    return EXIT_FAILURE;
  (*_se)->p=(IV2*)((char*)*_se+sizeof(SE));
  (*_se)->size=size;

  return EXIT_SUCCESS;
}

int seCircle(double radi,int centY,int centX,int origin,SE** _se) {
  int x,y,s;
  SE *se;

  int rradi = (int)(radi+0.5);

  for(y=-rradi,s=0;y<=rradi;y++)
    for(x=-rradi;x<=rradi;x++)
      if(!((sqrt((double)x*x+y*y)>radi) || (x-centX==0&&y-centY==0&&origin)))
        s++;
  if(semalloc(s,_se))
    return EXIT_FAILURE;
  se=*_se;
  se->max=0;
  for(y=-rradi,s=0;y<=rradi;y++)
    for(x=-rradi;x<=rradi;x++)
      if(!((sqrt((double)x*x+y*y)>radi) || (x-centX==0&&y-centY==0&&origin))) {
        se->p[s].x=x-centX;
        se->p[s].y=y-centY;
        if(abs(x-centX)>se->max)
          se->max=abs(x-centX);
        if(abs(y-centY)>se->max)
          se->max=abs(y-centY);
        s++;
      }

  return EXIT_SUCCESS;
}

int seRect(int seH,int seW,int centY,int centX,int origin,SE** _se)
{
  int x,y,s;
  SE *se;

  s=seH*seW;
  if(origin&&centY>=0&&centX>=0&&centY<seH&&centX<seW)
    s--;
  if(semalloc(s,_se))
    return EXIT_FAILURE;
  se=*_se;
  se->max=0;
  for(y=0,s=0;y<seH;y++)
    for(x=0;x<seW;x++)
      if(!(x-centX==0&&y-centY==0&&origin)) {
        se->p[s].x=x-centX;
        se->p[s].y=y-centY;
        if(abs(x-centX)>se->max)
          se->max=abs(x-centX);
        if(abs(y-centY)>se->max)
          se->max=abs(y-centY);
        s++;
      }

  return EXIT_SUCCESS;
}

inline int semax_graym(gray** img,int imgW,int imgH,SE* se,int x,int y) {
  int max = img[y][x];
  int n;
  for(n=se->size-1;n>=0;n--) {
    int p = x+se->p[n].x;
    int q = y+se->p[n].y;
    if(p>=0 && q>=0 && p<imgW && q<imgH)
      max = img[q][p]>max?img[q][p]:max;
  }
  return max;
}

inline int semin_graym(gray** img,int imgW,int imgH,SE* se,int x,int y) {
  int min = img[y][x];
  int n;
  for(n=se->size-1;n>=0;n--) {
    int p = x+se->p[n].x;
    int q = y+se->p[n].y;
    if(p>=0 && q>=0 && p<imgW && q<imgH)
      min = img[q][p]<min?img[q][p]:min;
  }
  return min;
}

int morphdilate_graym(gray** img, int imgW, int imgH, SE* se, gray** out) {
  gray **in = img;

  if(img==out) {
    if(malloc_graym(imgW,imgH,&in,0)) {
      fprintf(stderr,"morphdilate_graym: error: unable to reserve memory.\n");
      return EXIT_FAILURE;
    }
    int y;
    for(y=imgH-1;y>=0;y--) {
      int x;
      gray *iny = in[y];
      gray *imgy = img[y];
      for(x=imgW-1;x>=0;x--)
	iny[x] = imgy[x];
    }
  }

  int y;
  for(y=imgH-1;y>=0;y--) {
    int x;
    gray *outy = out[y];
    for(x=imgW-1;x>=0;x--)
      outy[x] = semax_graym(in,imgW,imgH,se,x,y);
  }

  if(img==out)
    free(in);

  return EXIT_SUCCESS;
}

int morpherode_graym(gray** img, int imgW, int imgH, SE* se, gray** out) {
  gray **in = img;

  if(img==out) {
    if(malloc_graym(imgW,imgH,&in,0)) {
      fprintf(stderr,"morpherode_graym: error: unable to reserve memory.\n");
      return EXIT_FAILURE;
    }
    int y;
    for(y=imgH-1;y>=0;y--) {
      int x;
      gray *iny = in[y];
      gray *imgy = img[y];
      for(x=imgW-1;x>=0;x--)
	iny[x] = imgy[x];
    }
  }

  int y;
  for(y=imgH-1;y>=0;y--) {
    int x;
    gray *outy = out[y];
    for(x=imgW-1;x>=0;x--)
      outy[x] = semin_graym(in,imgW,imgH,se,x,y);
  }

  if(img==out)
    free(in);

  return EXIT_SUCCESS;
}

int morphopen_graym(gray** img, int imgW, int imgH, SE* se, gray** tmp, gray** out) {
  gray **ltmp = tmp;

  if(tmp==NULL)
    if(malloc_graym(imgW,imgH,&ltmp,0)) {
      fprintf(stderr,"morphopen_graym: error: unable to reserve memory.\n");
      return EXIT_FAILURE;
    }

  morpherode_graym(img,imgW,imgH,se,ltmp);
  morphdilate_graym(ltmp,imgW,imgH,se,out);

  if(tmp==NULL)
    free(ltmp);

  return EXIT_SUCCESS;
}

int morphclose_graym(gray** img, int imgW, int imgH, SE* se, gray** tmp, gray** out) {
  gray **ltmp = tmp;

  if(tmp==NULL)
    if(malloc_graym(imgW,imgH,&ltmp,0)) {
      fprintf(stderr,"morphclose_graym: error: unable to reserve memory.\n");
      return EXIT_FAILURE;
    }

  morphdilate_graym(img,imgW,imgH,se,ltmp);
  morpherode_graym(ltmp,imgW,imgH,se,out);

  if(tmp==NULL)
    free(ltmp);

  return EXIT_SUCCESS;
}
