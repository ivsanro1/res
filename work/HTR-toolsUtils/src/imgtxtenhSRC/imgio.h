/*
    Functions for reading and writting images

    Copyright (C) 2004-2013 Mauricio Villegas <mauvilsa@upv.es>

    This file is part of imgtxtenh.

    imgtxtenh is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __MV_IMGIO_H__
#define __MV_IMGIO_H__

#include "mem.h"

#include <stdio.h>

typedef unsigned char gray;
typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} pixel;

#define IMGIO_GRAYMAX 255

#define limit_gray(a) (gray)(((a)<0?0:(a))>IMGIO_GRAYMAX?IMGIO_GRAYMAX:((a)<0?0:(a)))

#define malloc_graym(imW,imH,im,clr) mmem((imW),(imH),sizeof(gray),(clr),(char***)(im))
#define malloc_pixelm(imW,imH,im,clr) mmem((imW),(imH),sizeof(pixel),(clr),(char***)(im))
#define malloc_intm(R,C,M,clr) mmem((R),(C),sizeof(int),(clr),(char***)(M))
#define malloc_floatm(R,C,M,clr) mmem((R),(C),sizeof(float),(clr),(char***)(M))

#define clone_graym(im,imW,imH,clon) mclone((char**)(im),(imW),(imH),sizeof(gray),(char***)(clon))

int graym2pixelm(gray** img, int imgW, int imgH, pixel** out);
int rgbm2graym(pixel** img, int imgW, int imgH, gray** out);

int readimg_graym(char* fname, gray*** _img, int* _imgW, int* _imgH, char* caller);
int readimg_pixelm(char* fname, pixel*** _img, int* _imgW, int* _imgH, char* caller);
int fscan_graym(FILE* file, gray*** _img, int* _imgW, int* _imgH, char* caller);
int fscan_pixelm(FILE* file, pixel*** _img, int* _imgW, int* _imgH, char* caller);
int fscan_grayalphm(FILE* file, gray*** _img, gray*** _alph, int* _imgW, int* _imgH, char* caller);
int fscanimghead(FILE *file, int verbose, char *_format, int *_imgW, int *_imgH, int *_chann, char *_cspace, char* caller);
int fscanpnmhead(FILE *file, char *_format, int *_imgW, int *_imgH, int *_maxval);
int fscanpgm_graym(FILE* file, gray*** _img, int* _imgW, int* _imgH, gray* _maxval, char* caller);
int fscanppm_pixelm(FILE* file, pixel*** _img, int* _imgW, int* _imgH, gray* _maxval, char* caller);
int fscanbmp_pixelm(FILE* file, pixel*** _img, int* _imgW, int* _imgH, char* caller);
int fscanjpg_pixelm(FILE *file, pixel ***_img, int *_imgW, int *_imgH, char* caller);
int fscanpng_pixelm(FILE *file, pixel ***_img, int *_imgW, int *_imgH, char* caller);
int fscanpng_rgbam(FILE *file, pixel ***_img, gray ***_alph, int *_imgW, int *_imgH, char* caller);
int fscantif_pixelm(FILE *file, pixel ***_img, int *_imgW, int *_imgH, char* caller);

int writeimg_graym(char* fname, gray** img, int imgW, int imgH, char* caller);
int writeimg_pixelm(char* fname, pixel** img, int imgW, int imgH, char* caller);
int fprintpgm_graym(FILE* file, gray** img, int imgW, int imgH, gray maxval, char format);
int fprintppm_pixelm(FILE* file, pixel** img, int imgW, int imgH, gray maxval, char format);
int fprintbmp_pixelm(FILE* file, pixel** img, int imgW, int imgH);
int fprintjpg_graym(FILE *file, gray **img, int imgW, int imgH, int quality);
int fprintjpg_pixelm(FILE *file, pixel **img, int imgW, int imgH, int quality);
int fprintpng_graym(FILE *file, gray **img, gray **alph, int imgW, int imgH);
int fprintpng_pixelm(FILE *file, pixel **img, gray **alph, int imgW, int imgH);
int fprinttif_bitm(FILE *file, gray **img, int imgW, int imgH);
int fprinttif_graym(FILE *file, gray **img, int imgW, int imgH);
int fprinttif_pixelm(FILE *file, pixel **img, int imgW, int imgH);

#endif
