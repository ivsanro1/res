/**
    Functions related to integral images

    Copyright (C) 2004-2013 Mauricio Villegas <mauvilsa@upv.es>

    This file is part of imgtxtenh.

    imgtxtenh is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "intimg.h"

#include <stdlib.h>
#include <math.h>

int compII12_graym(gray** img, gray** alph, int imgW, int imgH, II1** ii1, II2** ii2, II1** cnt) {
  int x,y;

  if(imgW*imgH>66051 && sizeof(II2)<=4)
    fprintf(stderr,"compII12_graym: warning: posible II overflow.\n");

  if(alph!=NULL)
    for(y=0;y<imgH;y++)
      for(x=0;x<imgW;x++)
        if(alph[y][x]==0)
          img[y][x]=0;

  if(alph!=NULL)
    cnt[0][0] = alph[0][0] ? 1 : 0;
  ii1[0][0] = (II1)img[0][0];
  ii2[0][0] = (II2)img[0][0]*(II2)img[0][0];
  for(y=1;y<imgH;y++) {
    if(alph!=NULL)
      cnt[y][0] = cnt[y-1][0] + ( alph[y][0] ? 1 : 0 );
    ii1[y][0] = ii1[y-1][0]+(II1)img[y][0];
    ii2[y][0] = ii2[y-1][0]+(II2)img[y][0]*(II2)img[y][0];
  }
  for(x=1;x<imgW;x++) {
    if(alph!=NULL)
      cnt[0][x] = cnt[0][x-1] + ( alph[0][x] ? 1 : 0 );
    ii1[0][x] = ii1[0][x-1]+(II1)img[0][x];
    ii2[0][x] = ii2[0][x-1]+(II2)img[0][x]*(II2)img[0][x];
  }
  for(y=1;y<imgH;y++)
    for(x=1;x<imgW;x++) {
      if(alph!=NULL)
        cnt[y][x] = cnt[y-1][x]+cnt[y][x-1]-cnt[y-1][x-1] + ( alph[y][x] ? 1 : 0 );
      ii1[y][x] = ii1[y-1][x]+ii1[y][x-1]-ii1[y-1][x-1]+(II1)img[y][x];
      ii2[y][x] = ii2[y-1][x]+ii2[y][x-1]-ii2[y-1][x-1]+(II2)img[y][x]*(II2)img[y][x];
    }

  return EXIT_SUCCESS;
}

int mean_II(II1** ii1, II1** cnt, int xmin, int ymin, int cropW, int cropH, float* _mean) {
  int xminm1 = xmin-1;
  int yminm1 = ymin-1;
  int xmax = xminm1+cropW;
  int ymax = yminm1+cropH;

  II1 S1 = ii1[ymax][xmax];
  if(yminm1>-1) {
    S1 -= ii1[yminm1][xmax];
    if(xminm1>-1)
      S1 += ii1[yminm1][xminm1]-ii1[ymax][xminm1];
  }
  else if(xminm1>-1)
    S1 -= ii1[ymax][xminm1];

  int numpix;
  if(cnt!=NULL) {
    numpix = cnt[ymax][xmax];
    if(yminm1>-1) {
      numpix -= cnt[yminm1][xmax];
      if(xminm1>-1)
        numpix += cnt[yminm1][xminm1]-cnt[ymax][xminm1];
    }
    else if(xminm1>-1)
      numpix -= cnt[ymax][xminm1];
  }
  else
    numpix = cropW*cropH;

  *_mean = (float)((int)S1)/((float)numpix);

  return EXIT_SUCCESS;
}

int sd_II(II1** ii1, II2** ii2, II1** cnt, int xmin, int ymin, int cropW, int cropH, float* _sd) {
  int xminm1 = xmin-1;
  int yminm1 = ymin-1;
  int xmax = xminm1+cropW;
  int ymax = yminm1+cropH;

  II1 S1 = ii1[ymax][xmax];
  II2 S2 = ii2[ymax][xmax];
  if(yminm1>-1) {
    S1 -= ii1[yminm1][xmax];
    S2 -= ii2[yminm1][xmax];
    if(xminm1>-1) {
      S1 += ii1[yminm1][xminm1]-ii1[ymax][xminm1];
      S2 += ii2[yminm1][xminm1]-ii2[ymax][xminm1];
    }
  }
  else if(xminm1>-1) {
    S1 -= ii1[ymax][xminm1];
    S2 -= ii2[ymax][xminm1];
  }

  int numpix;
  if(cnt!=NULL) {
    numpix = cnt[ymax][xmax];
    if(yminm1>-1) {
      numpix -= cnt[yminm1][xmax];
      if(xminm1>-1)
        numpix += cnt[yminm1][xminm1]-cnt[ymax][xminm1];
    }
    else if(xminm1>-1)
      numpix -= cnt[ymax][xminm1];
  }
  else
    numpix = cropW*cropH;

  float mean = (float)S1/(float)numpix;
  if(((float)S2/(float)numpix - mean*mean) <= 0.0)
    *_sd = 0;
  else
    *_sd = sqrt((float)S2/(float)numpix - mean*mean);
 
  return EXIT_SUCCESS;
}

int meanSd_II(II1** ii1, II2** ii2, II1** cnt, int xmin, int ymin, int cropW, int cropH, float* _mean, float* _sd) {
  int xminm1 = xmin-1;
  int yminm1 = ymin-1;
  int xmax = xminm1+cropW;
  int ymax = yminm1+cropH;

  II1 S1 = ii1[ymax][xmax];
  II2 S2 = ii2[ymax][xmax];
  if(yminm1>-1) {
    S1 -= ii1[yminm1][xmax];
    S2 -= ii2[yminm1][xmax];
    if(xminm1>-1) {
      S1 += ii1[yminm1][xminm1]-ii1[ymax][xminm1];
      S2 += ii2[yminm1][xminm1]-ii2[ymax][xminm1];
    }
  }
  else if(xminm1>-1) {
    S1 -= ii1[ymax][xminm1];
    S2 -= ii2[ymax][xminm1];
  }

  int numpix;
  if(cnt!=NULL) {
    numpix = cnt[ymax][xmax];
    if(yminm1>-1) {
      numpix -= cnt[yminm1][xmax];
      if(xminm1>-1)
        numpix += cnt[yminm1][xminm1]-cnt[ymax][xminm1];
    }
    else if(xminm1>-1)
      numpix -= cnt[ymax][xminm1];
  }
  else
    numpix = cropW*cropH;

  *_mean = (float)((int)S1)/((float)numpix);
  if(((float)S2/(float)numpix-(*_mean)*(*_mean)) <= 0.0)
    *_sd = 0;
  else
    *_sd = sqrt((float)S2/(float)numpix-(*_mean)*(*_mean));

  return EXIT_SUCCESS;
}

int enhSauvola_graym(gray** img, gray** alph, int imgW, int imgH, II1*** _ii1, II2*** _ii2, II1*** _cnt, int winW, float prm, float slp) {

  II1 **cnt = NULL;

  if(*_ii1==NULL || *_ii2==NULL) {
    int err = 0;
    err += malloc_II1(imgW,imgH,_ii1,0);
    err += malloc_II2(imgW,imgH,_ii2,0);
    if(alph!=NULL) {
      err += malloc_II1(imgW,imgH,_cnt,0);
      cnt = *_cnt;
    }
    if(err) {
      fprintf(stderr,"enhSauvola_graym: error: unable to reserve memory\n");
      return EXIT_FAILURE;
    }
    compII12_graym(img,alph,imgW,imgH,*_ii1,*_ii2,cnt);
  }

  winW = winW/2;

  int y;
  for(y=0;y<imgH;y++) {
    gray *imgy = img[y];
    int ymin = y-winW;
    int ymax = y+winW;
    ymin = ymin<0?0:ymin;
    ymax = ymax>=imgH?imgH-1:ymax;
    int x;
    for(x=0;x<imgW;x++) {
      if(alph!=NULL && alph[y][x]==0)
        imgy[x] = 255;
      else {
        int xmin = x-winW;
        int xmax = x+winW;
        xmin = xmin<0?0:xmin;
        xmax = xmax>=imgW?imgW-1:xmax;
        float mu,sd;
        meanSd_II(*_ii1,*_ii2,cnt,xmin,ymin,xmax-xmin+1,ymax-ymin+1,&mu,&sd);
        float thr = mu*(1+prm*((sd/128)-1));
        if(slp==0.0) {
          if(imgy[x]>thr)
            imgy[x] = 255;
          else
            imgy[x] = 0;
        }
        else if(sd>1e-4) {
          float m = 255.0/(2*slp*sd);
          float c = 128-m*thr;
          imgy[x] = limit_gray(m*imgy[x]+c);
        }
      }
    }
  }

  return EXIT_SUCCESS;
}

int alphaExtend_graym(gray **img, gray **alph, int imgW, int imgH, int winW, float prm) {
  gray **tmp;
  int err = clone_graym(img,imgW,imgH,&tmp);
  if(err) {
    fprintf(stderr,"alphaExtend_graym: error: unable to reserve memory\n");
    return err;
  }

  II1** ii1 = NULL;
  II2** ii2 = NULL;
  err = enhSauvola_graym(tmp,NULL,imgW,imgH,&ii1,&ii2,NULL, winW, prm, 0);
  if(err!=EXIT_SUCCESS) {
    free(tmp);
    return err;
  }
  free(ii1);
  free(ii2);

  SE *se;
  err = seCircle(2,0,0,1,&se);
  if(err) {
    free(tmp);
    return err;
  }
  err = morpherode_graym(tmp,imgW,imgH,se,tmp);
  if(err!=EXIT_SUCCESS) {
    free(tmp);
    return err;
  }
  free(se);

  int n;
  for(n=imgW*imgH-1;n>=0;n--)
    if(alph[0][n]==0 && tmp[0][n]!=0)
      alph[0][n] = 255;

  free(tmp);

  return EXIT_SUCCESS;
}
