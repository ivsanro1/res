/*
    Tool for enhancing noisy scanned text images
   
    Copyright (C) 2004-2013 Mauricio Villegas <mauvilsa@upv.es>

    This file is part of imgtxtenh.

    imgtxtenh is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*** Includes *****************************************************************/
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "intimg.h"
#include "morph.h"

/*** Definitions **************************************************************/
static char tool[] = "imgtxtenh";

static char revnum[] = "$Revision: 144 $";
static char revdate[] = "$Date: 2013-05-21 10:55:02 +0200 (Tue, 21 May 2013) $";

int gb_winW = 30;
float gb_prm = 0.2;
float gb_slp = 0.5;
float gb_satu = -1;
int opaque = 0;

int asciipgm = 0;
int outtif = 0;
int outjpg = 0;
int outpng = 0;
int verbose = 0;

/*** Functions ****************************************************************/
void print_version(FILE *file)
{
  char ver[128];
  char *p;
  sprintf(ver,"%s: revision %s",tool,revnum+11);
  p=strchr(ver,'$');
  sprintf(p,"(%s",revdate+7);
  p=strchr(p+1,'(');
  *(p-1)=')';
  *p='\0';
  fprintf(file,"%s\n",ver);
}

void print_usage(FILE *file)
{
  char vsatu[20];
  sprintf(vsatu,"%g%%",gb_satu);

  fprintf(file,"Usage: %s [<options below>]\n",tool);
  fprintf(file,"\n");
  fprintf(file,"  -i ifile          Read image from 'ifile' (def.=stdin)\n");
  fprintf(file,"  -o ofile          Write output to 'ofile' (def.=stdout)\n");
  fprintf(file,"  -w <width:int>    Window width (def.=%d)\n",gb_winW);
  fprintf(file,"  -p <mfct:float>   Sauvola mean threshold factor (def.=%g)\n",gb_prm);
  fprintf(file,"  -s <sfct:float>   Threshold slope factor (def.=%g)\n",gb_slp);
  fprintf(file,"  -S <satu:float>   Contrast stretch saturating satu%% of pixels (def.=%s)\n",(gb_satu>=0?vsatu:"false"));
  fprintf(file,"  -O                Opaque input image, i.e. ignore alpha (def.=%s)\n",(opaque?"true":"false"));
  fprintf(file,"  -a                Output ascii pgm (def.=%s)\n",(asciipgm?"true":"false"));
  fprintf(file,"  -n                Output in png format (def.=%s)\n",(outpng?"true":"false"));
  fprintf(file,"  -j                Output in jpeg format (def.=%s)\n",(outjpg?"true":"false"));
  fprintf(file,"  -t                Output in tiff format (def.=%s)\n",(outtif?"true":"false"));
  fprintf(file,"  -v                Set verbosity (def.=%s)\n",(verbose?"true":"false"));
  fprintf(file,"  -l lfile          Output log file to 'lfile' (def.=stderr)\n");
  fprintf(file,"\n");
  print_version(file);
}

inline int hist_graym(gray **img, gray **alph, int imgW, int imgH, int *hist) {
  int n;
  for(n=255;n>=0;n--)
    hist[n] = 0;
  int cnt = 0;
  for(n=imgW*imgH-1;n>=0;n--)
    if(alph==NULL || alph[0][n]!=0) {
      hist[img[0][n]]++;
      cnt++;
    }
  return cnt;
}

void stretch_graym(gray **img, gray **alph, int imgW, int imgH, float satu) {
  int hist[256];
  int cnt = hist_graym(img,alph,imgW,imgH,hist);
  int thr = satu*cnt+0.5;
  thr = thr==0 ? 1 : thr;

  int min;
  cnt = hist[0];
  for(min=0,cnt=0;min<=255;min++) {
    cnt += hist[min];
    if(cnt>thr)
      break;
  }
  int max;
  cnt = hist[255];
  for(max=255,cnt=0;max>=0;max--) {
    cnt += hist[max];
    if(cnt>thr)
      break;
  }

  double fact = 255.0/(max-min);
  int n;
  for(n=imgW*imgH-1;n>=0;n--)
    img[0][n] = limit_gray( fact*(img[0][n]-min)+0.5 );
}

/*** Program ******************************************************************/
int main(int argc,char *argv[])
{
  FILE *logfile = stderr;
  FILE *ifd = stdin;
  FILE *ofd = stdout;
  int err = 0;

  /// Parse input arguments ///
  int n;
  while((n=getopt(argc,argv,"Ohvanjtl:i:o:w:p:s:S:"))!=-1)
    switch(n) {
    case 'i':
      if((ifd=fopen(optarg,"rb"))==NULL) {
	fprintf(logfile,"%s: error: unable to open %s file %s\n",tool,"input",optarg);
	return EXIT_FAILURE;
      }
      break;
    case 'o':
      if((ofd=fopen(optarg,"wb"))==NULL) {
	fprintf(logfile,"%s: error: unable to open %s file %s\n",tool,"output",optarg);
	return EXIT_FAILURE;
      }
      break;
    case 'l':
      if((logfile=fopen(optarg,"wb"))==NULL) {
	fprintf(logfile,"%s: error: unable to open %s file %s\n",tool,"log",optarg);
	return EXIT_FAILURE;
      }
      break;
    case 'w':
      gb_winW = atoi(optarg);
      break;
    case 'p':
      gb_prm = atof(optarg);
      break;
    case 's':
      gb_slp = atof(optarg);
      break;
    case 'S':
      gb_satu = atof(optarg);
      break;
    case 'O':
      opaque = !opaque;
      break;
    case 'a':
      asciipgm = !asciipgm;
      break;
    case 'n':
      outpng = !outpng;
      break;
    case 'j':
      outjpg = !outjpg;
      break;
    case 't':
      outtif = !outtif;
      break;
    case 'v':
      verbose = !verbose;
      break;
    default:
      fprintf(logfile,"%s: error: incorrect input argument\n",tool);
      err = EXIT_FAILURE;
    case 'h':
      print_usage(logfile);
      return err;
    }

  /// Read input image ///
  gray **img;
  int imgW;
  int imgH;
  gray **alph = NULL;

  if(opaque)
    err = fscan_graym(ifd,&img,&imgW,&imgH,tool);
  else
    err = fscan_grayalphm(ifd,&img,&alph,&imgW,&imgH,tool);
  if(ifd!=stdin)
    fclose(ifd);
  if(err) {
    fprintf(logfile,"%s: error: problems reading input image\n",tool);
    return EXIT_FAILURE;
  }
  if(verbose)
    fprintf(logfile,"%s: read image of size %dx%d\n",tool,imgW,imgH);

  if(gb_satu>=0) {
    if(verbose)
      fprintf(logfile,"%s: streatching contrast saturating %g%%\n",tool,gb_satu);
    stretch_graym(img,alph,imgW,imgH,gb_satu/200);
  }

  /// Process alpha channel ///
  gray **talph = alph;
  if(alph!=NULL) {
    int err = clone_graym(talph,imgW,imgH,&alph);
    if(err!=EXIT_SUCCESS)
      return err;
    alphaExtend_graym(img,alph,imgW,imgH,gb_winW,gb_prm);
  }

  /// Enhance image ///
  II1** ii1 = NULL;
  II2** ii2 = NULL;
  II1** cnt = NULL;
  if(verbose)
    fprintf(logfile,"%s: enhancing by Sauvola (width=%d,mfct=%g,sfct=%g)\n",tool,gb_winW,gb_prm,gb_slp);
  enhSauvola_graym(img,alph,imgW,imgH,&ii1,&ii2,&cnt, gb_winW, gb_prm, gb_slp);

  /// Set transparent zones to white ///
  if(alph!=NULL) {
    free(alph);
    alph = talph;
    int n;
    for(n=imgW*imgH-1;n>=0;n--)
      if(alph[0][n]==0)
        img[0][n] = 255;
  }

  /// Output resulting image ///
  if(outpng)
    fprintpng_graym(ofd,img,alph,imgW,imgH);
  else if(outjpg)
    fprintjpg_graym(ofd,img,imgW,imgH,90);
  else if(outtif) {
    if(gb_slp==0.0)
      fprinttif_bitm(ofd,img,imgW,imgH);
    else
      fprinttif_graym(ofd,img,imgW,imgH);
  }
  else if(asciipgm)
    fprintpgm_graym(ofd,img,imgW,imgH,255,'2');
  else
    fprintpgm_graym(ofd,img,imgW,imgH,255,'5');

  /// Release resources ///
  if(ofd!=stdout)
    fclose(ofd);

  free(img);
  free(ii1);
  free(ii2);
  if(alph!=NULL) {
    free(alph);
    free(cnt);
  }

  return EXIT_SUCCESS;
}
