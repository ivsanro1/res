/*
    Functions for allocating memory
   
    Copyright (C) 2004-2013 Mauricio Villegas <mauvilsa@upv.es>

    This file is part of imgtxtenh.

    imgtxtenh is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void nfree(void** p) {
  if(p[0]!=NULL) {
    free(p[0]);
    p[0]=NULL;
  } else
    fprintf(stderr,"nfree: warning: tryed to free a null pointer\n");
}

int mem(int size,int clr,char** _p)
{
  char *p;

  if((p=(char*)malloc(size))==NULL)
    return EXIT_FAILURE;
  if(clr)
    memset(p,0,size);
  *_p=p;

  return EXIT_SUCCESS;
}

int mmem(int R,int C,int size,int clr,char*** _mat)
{
  int c;
  char **mat,*vec;

  mat=(char**)malloc(C*sizeof(char*)+R*C*size);
  if(mat==NULL)
    return EXIT_FAILURE;
  vec=(char*)(mat+C);
  for(c=0;c<C;c++,vec+=R*size)
    mat[c]=vec;
  if(clr)
    memset(mat[0],0,R*C*size);

  *_mat=mat;
  return EXIT_SUCCESS;
}

int vrmem(int* R,int C,int size,int clr,char*** _mat)
{
  int c;
  char **mat,*vec;

  int TR=0;
  for(c=0;c<C;c++)
    TR+=R[c];

  mat=(char**)malloc(C*sizeof(char*)+TR*C*size);
  if(mat==NULL)
    return EXIT_FAILURE;
  vec=(char*)(mat+C);
  for(c=0;c<C;vec+=R[c]*size,c++)
    mat[c]=vec;
  if(clr)
    memset(mat[0],0,TR*C*size);

  *_mat=mat;
  return EXIT_SUCCESS;
}

int bmem(int R,int C,int size,int clr,int brd,char*** _mat)
{
  int c;
  char **mat,*vec;

  mat=(char**)malloc((C+2*brd)*sizeof(char*)+(R+2*brd)*(C+2*brd)*size);
  if(mat==NULL)
    return EXIT_FAILURE;
  vec=(char*)(mat+C+2*brd)+brd*size;
  for(c=0;c<C+2*brd;c++,vec+=(R+2*brd)*size)
    mat[c]=vec;
  if(clr)
    memset((char*)(mat+C+2*brd),0,(R+2*brd)*(C+2*brd)*size);

  *_mat=mat+brd;
  return EXIT_SUCCESS;
}

int tmem(int D,int size,int clr,char*** _mat)
{
  int d;
  char **mat,*vec;

  mat=(char**)malloc(D*sizeof(char*)+size*D*(D+1)/2);
  if(mat==NULL)
    return EXIT_FAILURE;
  vec=(char*)(mat+D);
  for(d=0;d<D;d++,vec+=d*size)
    mat[d]=vec;
  if(clr)
    memset(mat[0],0,size*D*(D+1)/2);

  *_mat=mat;
  return EXIT_SUCCESS;
}

int mclone(char** mat, int R, int C, int size, char*** _clon) {
  int err = mmem(R,C,size,0,_clon);
  if(err!=EXIT_SUCCESS)
    return err;

  memcpy(_clon[0][0],mat[0],R*C*size);

  return EXIT_SUCCESS;
}
