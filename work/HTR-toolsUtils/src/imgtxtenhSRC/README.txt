Tool: imgtxtenh
Author: Mauricio Villegas <mauvilsa@upv.es>

Description:

  The function of 'imgtxtenh' is to clean/enhance noisy scanned text
  images, which could be printed or handwritten text. It takes as
  input an image and generates as output another image of the same
  size. Several input/output image formats are supported, namely: PGM,
  PPM, BMP, JPEG, PNG and TIFF.

  Currently only two techniques are implemented, which are the method
  of Sauvola [1], and a simple unpublished modification of the method
  of Sauvola (proposed by M. Villegas) that generates a grayscale
  image instead of binary foreground-background decisions. For
  explanation of the usage, execute:
    ./imgtxtenh -h

References:

[1] Sauvola, J. and Pietikainen, M. Adaptive document image
    binarization. Pattern Recognition, 33(2):225–236, 2000.
