/*
    Functions for allocating memory
   
    Copyright (C) 2004-2013 Mauricio Villegas <mauvilsa@upv.es>

    This file is part of imgtxtenh.

    imgtxtenh is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MV_MEM_H__
#define __MV_MEM_H__

#define bfree(M,b) free((char**)M-b)

#define malloc_intv(size,vec,clr) mem((size)*sizeof(int),clr,(char**)(vec))
#define malloc_doublev(size,vec,clr) mem((size)*sizeof(double),clr,(char**)(vec))

void nfree(void** p);
int mem(int size,int clr,char** _p);
int mmem(int R,int C,int size,int clr,char*** _mat);
int vrmem(int* R,int C,int size,int clr,char*** _mat);
int bmem(int R,int C,int size,int clr,int brd,char*** _mat);
int tmem(int D,int size,int clr,char*** _mat);
int mclone(char** mat, int R, int C, int size, char*** _clon);

#endif
