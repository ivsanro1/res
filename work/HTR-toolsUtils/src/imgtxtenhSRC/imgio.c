/*
    Functions for reading and writting images
    
    Copyright (C) 2004-2013 Mauricio Villegas <mauvilsa@upv.es>

    This file is part of imgtxtenh.

    imgtxtenh is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "imgio.h"

#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <jpeglib.h>
#include <png.h>
#include <tiffio.h>

#define LINEMAX 70
#define COMMENTMAX 128
#define EXTMAX 4
#define DEFAULT_PGM_FORMAT '5'
#define DEFAULT_PPM_FORMAT '6'
#define DEFAULT_JPEG_QUALITY 90

#define IMGIO_BITMAP     'b'
#define IMGIO_GRAY       'g'
#define IMGIO_GRAY_ALPHA 'G'
#define IMGIO_RGB        'r'
#define IMGIO_RGB_ALPHA  'R'
#define IMGIO_PALETTE    'p'

int graym2pixelm(gray** img, int imgW, int imgH, pixel** out) {
  int y;
  for(y=0;y<imgH;y++) {
    gray *imgy = img[y];
    pixel *outy = out[y];
    int x;
    for(x=0;x<imgW;x++) {
      outy[x].r = imgy[x];
      outy[x].g = imgy[x];
      outy[x].b = imgy[x];
    }
  }

  return EXIT_SUCCESS;
}

int rgbm2graym(pixel** img, int imgW, int imgH, gray** out) {
  int y;
  for(y=0;y<imgH;y++) {
    pixel *imgy = img[y];
    gray *outy = out[y];
    int x;
    for(x=0;x<imgW;x++)
      outy[x] = (gray)(0.299*imgy[x].r+0.587*imgy[x].g+0.114*imgy[x].b);
  }

  return EXIT_SUCCESS;
}

int readimg_graym(char* fname, gray*** _img, int* _imgW, int* _imgH, char* caller) {
  int err;
  FILE *file;
  if((file=fopen(fname,"rb"))==NULL) {
    fprintf(stderr,"%s: readimg_graym: error: unable to open file %s for reading\n",caller,fname);
    return EXIT_FAILURE;
  }
  err = fscan_graym(file,_img,_imgW,_imgH,caller);
  fclose(file);
  return err;
}

int readimg_pixelm(char* fname, pixel*** _img, int* _imgW, int* _imgH, char* caller) {
  int err;
  FILE *file;
  if((file=fopen(fname,"rb"))==NULL) {
    fprintf(stderr,"%s: readimg_pixelm: error: unable to open file %s for reading\n",caller,fname);
    return EXIT_FAILURE;
  }
  err = fscan_pixelm(file,_img,_imgW,_imgH,caller);
  fclose(file);
  return err;
}

int fscan_grayalphm(FILE* file, gray*** _img, gray*** _alph, int* _imgW, int* _imgH, char* caller) {
  char format[4];
  int err = 0;
  pixel** cimg = NULL;
  gray maxval;

  *_img = NULL;
  *_alph = NULL;

  err = fscanimghead(file,1,format,_imgW,_imgH,NULL,NULL,caller);
  if(err && err!='f') {
    fprintf(stderr,"%s: fscan_grayalphm: error: image format %s not supported\n",caller,format);
    return err;
  }
  if(err!='f' && (*_imgW<=0 || *_imgH<=0)) {
    fprintf(stderr,"%s: fscan_grayalphm: error: unexpected image size (%dx%d)\n",caller,*_imgW,*_imgH);
    return EXIT_FAILURE;
  }

  if(!strncmp(format,"pgm",3))
    return fscanpgm_graym(file,_img,_imgW,_imgH,&maxval,caller);
  else if(!strncmp(format,"jpg",3))
    err = fscanjpg_pixelm(file,&cimg,_imgW,_imgH,caller);
  else if(!strncmp(format,"bmp",3) || !strncmp(format,"dib",3))
    err = fscanbmp_pixelm(file,&cimg,_imgW,_imgH,caller);
  else if(!strncmp(format,"ppm",3))
    err = fscanppm_pixelm(file,&cimg,_imgW,_imgH,&maxval,caller);
  else if(!strncmp(format,"png",3))
    err = fscanpng_rgbam(file,&cimg,_alph,_imgW,_imgH,caller);
  else if(!strncmp(format,"tif",3))
    err = fscantif_pixelm(file,&cimg,_imgW,_imgH,caller);
  else {
    fprintf(stderr,"%s: fscan_grayalphm: error: image format %s not supported\n",caller,format);
    return EXIT_FAILURE;
  }
  if(err!=EXIT_SUCCESS)
    return err;

  if(*_alph==NULL)
    fprintf(stderr,"%s: fscan_grayalphm: warning: image does not have alpha channel\n",caller);

  if(cimg!=NULL) {
    err = malloc_graym(*_imgW,*_imgH,_img,0);
    if(err!=EXIT_SUCCESS)
      return err;
    err = rgbm2graym(cimg,*_imgW,*_imgH,*_img);
    if(err!=EXIT_SUCCESS)
      return err;
    free(cimg);
  }

  return EXIT_SUCCESS;
}

int fscan_graym(FILE* file, gray*** _img, int* _imgW, int* _imgH, char* caller) {
  char format[4];
  int err = 0;
  pixel** cimg = NULL;
  gray maxval;

  *_img = NULL;

  err = fscanimghead(file,1,format,_imgW,_imgH,NULL,NULL,caller);
  if(err && err!='f') {
    fprintf(stderr,"%s: fscan_graym: error: image format %s not supported\n",caller,format);
    return err;
  }
  if(err!='f' && (*_imgW<=0 || *_imgH<=0)) {
    fprintf(stderr,"%s: fscan_graym: error: unexpected image size (%dx%d)\n",caller,*_imgW,*_imgH);
    return EXIT_FAILURE;
  }

  if(!strncmp(format,"pgm",3))
    return fscanpgm_graym(file,_img,_imgW,_imgH,&maxval,caller);
  else if(!strncmp(format,"jpg",3))
    err = fscanjpg_pixelm(file,&cimg,_imgW,_imgH,caller);
  else if(!strncmp(format,"bmp",3) || !strncmp(format,"dib",3))
    err = fscanbmp_pixelm(file,&cimg,_imgW,_imgH,caller);
  else if(!strncmp(format,"ppm",3))
    err = fscanppm_pixelm(file,&cimg,_imgW,_imgH,&maxval,caller);
  else if(!strncmp(format,"png",3))
    err = fscanpng_pixelm(file,&cimg,_imgW,_imgH,caller);
  else if(!strncmp(format,"tif",3))
    err = fscantif_pixelm(file,&cimg,_imgW,_imgH,caller);
  else {
    fprintf(stderr,"%s: fscan_graym: error: image format %s not supported\n",caller,format);
    return EXIT_FAILURE;
  }
  if(err!=EXIT_SUCCESS)
    return err;

  if(cimg!=NULL) {
    err = malloc_graym(*_imgW,*_imgH,_img,0);
    if(err!=EXIT_SUCCESS)
      return err;
    err = rgbm2graym(cimg,*_imgW,*_imgH,*_img);
    if(err!=EXIT_SUCCESS)
      return err;
    free(cimg);
  }

  return EXIT_SUCCESS;
}

int fscan_pixelm(FILE* file, pixel*** _img, int* _imgW, int* _imgH, char* caller) {
  char format[4];
  int err = 0;
  gray** gimg = NULL;
  gray maxval;

  *_img = NULL;

  err = fscanimghead(file,1,format,_imgW,_imgH,NULL,NULL,caller);
  if(err && err!='f') {
    fprintf(stderr,"%s: fscan_pixelm: error: image format %s not supported\n",caller,format);
    return err;
  }
  if(err!='f' && (*_imgW<=0 || *_imgH<=0)) {
    fprintf(stderr,"%s: fscan_pixelm: error: unexpected image size (%dx%d)\n",caller,*_imgW,*_imgH);
    return EXIT_FAILURE;
  }

  if(!strncmp(format,"pgm",3))
    return fscanpgm_graym(file,&gimg,_imgW,_imgH,&maxval,caller);
  else if(!strncmp(format,"jpg",3))
    err = fscanjpg_pixelm(file,_img,_imgW,_imgH,caller);
  else if(!strncmp(format,"bmp",3) || !strncmp(format,"dib",3))
    err = fscanbmp_pixelm(file,_img,_imgW,_imgH,caller);
  else if(!strncmp(format,"ppm",3))
    err = fscanppm_pixelm(file,_img,_imgW,_imgH,&maxval,caller);
  else if(!strncmp(format,"png",3))
    err = fscanpng_pixelm(file,_img,_imgW,_imgH,caller);
  else if(!strncmp(format,"tif",3))
    err = fscantif_pixelm(file,_img,_imgW,_imgH,caller);
  else {
    fprintf(stderr,"%s: fscan_pixelm: error: image format %s not supported\n",caller,format);
    return EXIT_FAILURE;
  }
  if(err!=EXIT_SUCCESS)
    return err;

  if(gimg!=NULL) {
    err = malloc_pixelm(*_imgW,*_imgH,_img,0);
    if(err!=EXIT_SUCCESS)
      return err;
    err = graym2pixelm(gimg,*_imgW,*_imgH,*_img);
    if(err!=EXIT_SUCCESS)
      return err;
    free(gimg);
  }

  return EXIT_SUCCESS;
}

int fscanimghead(FILE *file, int verbose, char *_format, int *_imgW, int *_imgH, int *_chann, char *_cspace, char* caller) {
  //char buffer[COMMENTMAX];
  unsigned char buffer[COMMENTMAX];
  int imgW,imgH,chann=0;
  long initpos = ftell(file);
  char cspace = '\0';

  if(initpos==-1 && file!=stdin) {
    if(verbose)
      fprintf(stderr,"%s: fscanimghead: error: unable seek in stream\n",caller);
    return EXIT_FAILURE;
  }

  int cnt = 0;
  if(file==stdin) {
    int ch0 = getc(file);
    int ch1 = getc(file);
    if(ch0==-1 || ch1==-1) {
      if(verbose)
        fprintf(stderr,"%s: fscanimghead: error: problems reading stream\n",caller);
      return EXIT_FAILURE;
    }
    buffer[0] = ch0;
    buffer[1] = ch1;
    cnt = 2;
  }
  else {
    cnt = fread(buffer,1,2,file);
    if(cnt!=2) {
      if(verbose)
	fprintf(stderr,"%s: fscanimghead: error: problems reading stream\n",caller);
      return EXIT_FAILURE;
    }
  }

  if(buffer[0]=='P' && buffer[1]>='1' && buffer[1]<='6') {
    if(buffer[1]=='1' || buffer[1]=='4') {
      cspace=IMGIO_BITMAP;
      chann=1;
      if(_format!=NULL)
        sprintf(_format,"pbm");
    }
    else if(buffer[1]=='2' || buffer[1]=='5') {
      cspace=IMGIO_GRAY;
      chann=1;
      if(_format!=NULL)
        sprintf(_format,"pgm");
    }
    else if(buffer[1]=='3' || buffer[1]=='6') {
      cspace=IMGIO_RGB;
      chann=3;
      if(_format!=NULL)
        sprintf(_format,"ppm");
    }
    if(initpos!=-1) {
      while(getc(file)!='\n');
      int ch = getc(file);
      while(ch=='#') {
	while(getc(file)!='\n');
	ch = getc(file);
      }
      if((char)ungetc(ch,file)!=ch)
	return EXIT_FAILURE;
      // long p;
      // while(1) {
      // 	p=ftell(file);
      // 	if(fgets(buffer,COMMENTMAX,file)!=NULL)
      // 	  cnt++;
      // 	if(!(buffer[0]=='#' || buffer[0]==' ' || buffer[0]=='\n' || buffer[0]=='\r')) {
      // 	  p-=ftell(file);
      // 	  fseek(file,p,SEEK_CUR);
      // 	  break;
      // 	}
      // }
      int maxval;
      if(fscanf(file,"%d%d%d",&imgW,&imgH,&maxval)!=3) {
	if(verbose)
	  fprintf(stderr,"%s: fscanimghead: error: bad pnm header\n",caller);
	return EXIT_FAILURE;
      }
    }
  }

  else if(buffer[0]=='B' && buffer[1]=='M') {
    if(_format!=NULL)
      sprintf(_format,"bmp");
    if(initpos!=-1) {
      cnt+=fread(buffer,16,1,file);     // Ignore 16 bytes
      cnt+=fread(&imgW,4,1,file);       // Image width
      cnt+=fread(&imgH,4,1,file);       // Image height
      cnt+=fread(buffer,2,1,file);      // Ignore 2 bytes
      short int bitdepth;
      cnt+=fread(&bitdepth,2,1,file);   // Bit depth
      if(bitdepth==1) {
	cspace=IMGIO_BITMAP;
	chann=1;
      }
      else if(bitdepth==8) {
	cspace=IMGIO_GRAY;
	chann=1;
      }
      else if(bitdepth==24) {
	cspace=IMGIO_RGB;
	chann=3;
      }
    }
  }

  else if((unsigned char)buffer[0]==0xFF && (unsigned char)buffer[1]==0xD8) {
    if(_format!=NULL)
      sprintf(_format,"jpg");

    if(initpos!=-1) {
      cnt+=fread(buffer,2,1,file);
      if( !((unsigned char)buffer[0]==0xFF && (unsigned char)buffer[1]==0xE0) && // JFIF format
	  !((unsigned char)buffer[0]==0xFF && (unsigned char)buffer[1]==0xE1) ) { // Exif format
	if(verbose)
	  fprintf(stderr,"%s: fscanimghead: error: only JFIF/Exif jpeg formats supported\n",caller);
	return EXIT_FAILURE;
      }

      fseek(file,initpos,SEEK_SET);

      struct jpeg_decompress_struct cinfo;
      struct jpeg_error_mgr jerr;

      cinfo.err = jpeg_std_error(&jerr);
      jpeg_create_decompress(&cinfo);
      jpeg_stdio_src(&cinfo, file);
      (void)jpeg_read_header(&cinfo, TRUE);
      (void)jpeg_start_decompress(&cinfo);

      imgW=cinfo.output_width;
      imgH=cinfo.output_height;
      chann=cinfo.output_components;
      if(chann==3)
	cspace=IMGIO_RGB;
      else if(chann==1)
	cspace=IMGIO_GRAY;

      jpeg_destroy_decompress(&cinfo);
    }
  }

  else if((unsigned char)buffer[0]==0x89 && buffer[1]=='P') {
    if(_format!=NULL)
      sprintf(_format,"png");

    if(initpos!=-1) {
      cnt+=fread(buffer,2,1,file);
      if(!(buffer[0]=='N' && buffer[1]=='G')) {
	if(verbose)
	  fprintf(stderr,"%s: fscanimghead: error: bad png header\n",caller);
	return EXIT_FAILURE;
      }

      fseek(file,initpos,SEEK_SET);

      png_byte bit_depth;
      png_byte color_type;
      png_structp png_ptr;
      png_infop info_ptr;

      png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
      info_ptr = png_create_info_struct(png_ptr);
      if(setjmp(png_jmpbuf(png_ptr))) {
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	return EXIT_FAILURE;
      }

      png_init_io(png_ptr, file);
      png_read_info(png_ptr, info_ptr);
      imgW = png_get_image_width(png_ptr, info_ptr);
      imgH = png_get_image_height(png_ptr, info_ptr);
      chann = (int)png_get_channels(png_ptr, info_ptr);
      color_type = png_get_color_type(png_ptr, info_ptr);
      bit_depth = png_get_bit_depth(png_ptr, info_ptr);

      //if(chann==3 && color_type==PNG_COLOR_TYPE_RGB)
      //  cspace=IMGIO_RGB;
      //else if(chann==1 && color_type==PNG_COLOR_TYPE_GRAY)
      //  cspace=IMGIO_GRAY;
      if(color_type==IMGIO_GRAY && bit_depth==1)
        cspace=IMGIO_BITMAP;
      else
      switch(color_type) {
      case PNG_COLOR_TYPE_GRAY:       cspace=IMGIO_GRAY;       break;
      case PNG_COLOR_TYPE_GRAY_ALPHA: cspace=IMGIO_GRAY_ALPHA; break;
      case PNG_COLOR_TYPE_RGB:        cspace=IMGIO_RGB;        break;
      case PNG_COLOR_TYPE_RGB_ALPHA:  cspace=IMGIO_RGB_ALPHA;  break;
      case PNG_COLOR_TYPE_PALETTE:    cspace=IMGIO_PALETTE;    break;
      }

      //png_read_end(png_ptr, NULL);
      //png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    }
  }

  else if( (buffer[0]=='I' && buffer[1]=='I') || (buffer[0]=='M' && buffer[1]=='M') ) {
    sprintf(_format,"tif");
    initpos = -1;
  }

  else if(buffer[0]=='G' && buffer[1]=='I') {
    sprintf(_format,"gif");
    return EXIT_FAILURE;
  }
  else {
    sprintf(_format,"???");
    return EXIT_FAILURE;
  }

  if(initpos!=-1) {
    if(_imgW!=NULL)
      *_imgW=imgW;
    if(_imgH!=NULL)
      *_imgH=imgH;
    if(_chann!=NULL)
      *_chann=chann;
    if(_cspace!=NULL) {
      switch(cspace) {
      case IMGIO_BITMAP:     sprintf(_cspace,"BW"); break;
      case IMGIO_GRAY:       sprintf(_cspace,"GRAY"); break;
      case IMGIO_GRAY_ALPHA: sprintf(_cspace,"GA"); break;
      case IMGIO_RGB:        sprintf(_cspace,"RGB");break;
      case IMGIO_RGB_ALPHA:  sprintf(_cspace,"RGBA"); break;
      case IMGIO_PALETTE:    sprintf(_cspace,"PALE"); break;
      }
      //if(cspace==IMGIO_BITMAP)
      //  sprintf(_cspace,"BW");
      //if(cspace==IMGIO_GRAY)
      //  sprintf(_cspace,"gray");
      //if(cspace==IMGIO_RGB)
      //  sprintf(_cspace,"RGB");
    }

    fseek(file,initpos,SEEK_SET);
    cnt = EXIT_SUCCESS;
  }
  else {
    cnt = 0;
    if((ungetc(buffer[1],file))!=buffer[1])
      cnt++;
    if((ungetc(buffer[0],file))!=buffer[0])
      cnt++;
    if(cnt) {
      if(verbose)
        fprintf(stderr,"%s: fscanimghead: error: unable to ungetc twice\n",caller);
      return EXIT_FAILURE;
    }
    cnt = 'f';
  }

  return cnt;
}

int fscanpnmhead(FILE *file, char *_format, int *_imgW, int *_imgH, int *_maxval) {
  // char comment[COMMENTMAX];
  // long p;
  // int cnt=0;

  if(getc(file)!='P')
    return EXIT_FAILURE;
  *_format = getc(file);
  while(getc(file)!='\n');
  int ch = getc(file);
  while(ch=='#') {
    while(getc(file)!='\n');
    ch = getc(file);
  }
  if((char)ungetc(ch,file)!=ch)
    return EXIT_FAILURE;
  // while(1) {
  //   p = ftell(file);
  //   if(fgets(comment,COMMENTMAX,file)!=NULL)
  //     cnt++;
  //   if(!(comment[0]=='#' || comment[0]==' ' || comment[0]=='\n' || comment[0]=='\r')) {
  //     p -= ftell(file);
  //     fseek(file,p,SEEK_CUR);
  //     break;
  //   }
  // }
  if(fscanf(file,"%d%d%d",_imgW,_imgH,_maxval)!=3)
    return EXIT_FAILURE;
  while(getc(file)!='\n');
  // if(fgets(comment,COMMENTMAX,file)!=NULL)
  //   cnt++;

  return EXIT_SUCCESS;
}

int fscanpgm_graym(FILE* file, gray*** _img, int* _imgW, int* _imgH, gray* _maxval, char* caller) {
  int x,y,z,imgW,imgH,aux;
  char format;

  if(fscanpnmhead(file,&format,&imgW,&imgH,&aux)) {
    fprintf(stderr,"%s: fscanpgm_graym: error: bad pnm header\n",caller);
    return EXIT_FAILURE;
  }
  if(!(format=='2'||format=='5')) {
    fprintf(stderr,"%s: fscanpgm_graym: error: format 'graymap text(P2)' or 'graymap raw(P5)' expected\n",caller);
    return EXIT_FAILURE;
  }
  if(aux>255) {
    fprintf(stderr,"%s: fscanpgm_graym: error: maximum gray value greater than 255 is not supported\n",caller);
    return EXIT_FAILURE;
  }
  if(malloc_graym(imgW,imgH,_img,0)) {
    fprintf(stderr,"%s: fscanpgm_graym: error: unable to reserve memory\n",caller);
    return EXIT_FAILURE;
  }

  *_imgW = imgW;
  *_imgH = imgH;
  *_maxval = (gray)aux;
  aux = imgW*imgH;
  if(format=='2') {
    for(y=0,z=0;y<imgH;y++)
      for(x=0;x<imgW;x++)
        z += fscanf(file,"%hhd",&(_img[0][y][x]));
  }
  else
    z = fread(_img[0][0],sizeof(gray),aux,file);

  if(z!=aux) {
    fprintf(stderr,"%s: fscanpgm_graym: warning: image appears to be truncated\n",caller);
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

int fscanppm_pixelm(FILE* file, pixel*** _img, int* _imgW, int* _imgH, gray* _maxval, char* caller) {
  int x,y,z,imgW,imgH,aux;
  char format;

  if(fscanpnmhead(file,&format,&imgW,&imgH,&aux)) {
    fprintf(stderr,"%s: fscanpgm_graym: error: bad pnm header\n",caller);
    return EXIT_FAILURE;
  }
  if(!(format=='3'||format=='6')) {
    fprintf(stderr,"%s: fscanppm_pixelm: error: format 'colormap text(P3)' or 'colormap raw(P6)' expected\n",caller);
    return EXIT_FAILURE;
  }
  if(aux>255) {
    fprintf(stderr,"%s: fscanppm_pixelm: error: maximum gray value greater than 255 is not supported\n",caller);
    return EXIT_FAILURE;
  }
  if(malloc_pixelm(imgW,imgH,_img,0)) {
    fprintf(stderr,"%s: fscanppm_pixelm: error: unable to reserve memory\n",caller);
    return EXIT_FAILURE;
  }

  *_imgW = imgW;
  *_imgH = imgH;
  *_maxval = (gray)aux;
  aux = 3*imgW*imgH;
  if(format=='3') {
    for(y=0,z=0;y<imgH;y++)
      for(x=0;x<imgW;x++)
        z += fscanf(file,"%hhd%hhd%hhd",&(_img[0][y][x].r),&(_img[0][y][x].g),&(_img[0][y][x].b));
  }
  else
    z = fread(_img[0][0],sizeof(gray),aux,file);

  if(z!=aux) {
    fprintf(stderr,"%s: fscanppm_pixelm: warning: image appears to be truncated\n",caller);
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

int fscanbmp_pixelm(FILE* file, pixel*** _img, int* _imgW, int* _imgH, char* caller) {
  int x,y,waste,colors;
  short int bitdepth;
  char buffer[16];
  gray val;
  pixel *T;

  fprintf(stderr,"%s: fscanbmp_pixelm: warning: function not working correctly\n",caller);

  int cnt=fread(buffer,2,1,file);      // BMP format id
  if(buffer[0]!='B' || buffer[1]!='M') {
    fprintf(stderr,"%s: fscanbmp_pixelm: error: file is not in bmp format\n",caller);
    return EXIT_FAILURE;
  }
  cnt+=fread(buffer,16,1,file);     // Ignore 16 bytes
  cnt+=fread(_imgW,4,1,file);       // Image width
  cnt+=fread(_imgH,4,1,file);       // Image height
  cnt+=fread(buffer,2,1,file);      // Ignore 2 bytes
  cnt+=fread(&bitdepth,2,1,file);   // Bit depth
  cnt+=fread(&x,4,1,file);          // Compression
  if(x) {
    fprintf(stderr,"%s: fscanbmp_pixelm: error: bmp format with compression not supported\n",caller);
    return EXIT_FAILURE;
  }
  cnt+=fread(buffer,12,1,file);     // Ignore 12 bytes
  cnt+=fread(&colors,4,1,file);     // Colors used
  cnt+=fread(buffer,4,1,file);      // Ignore 4 bytes
  if(malloc_pixelm(*_imgW,*_imgH,_img,0)) {
    fprintf(stderr,"%s: fscanbmp_pixelm: error: unable to reserve memory\n",caller);
    return EXIT_FAILURE;
  }
  if(bitdepth==24) {
    waste = ((*_imgW)*3)%4;
    if(waste)
      waste = 4-waste;
    for(y=(*_imgH)-1;y>=0;y--) {
      for(x=0;x<*_imgW;x++) {
        _img[0][y][x].b = getc(file);
        _img[0][y][x].g = getc(file);
        _img[0][y][x].r = getc(file);
      }
      cnt+=fread(buffer,waste,1,file);
    }
  }
  else if(bitdepth==8) {
    if(colors==0)
      colors = 256;
    T = (pixel*)malloc(colors*sizeof(pixel));
    for(x=0;x<colors;x++) {
      T[x].b = getc(file);
      T[x].g = getc(file);
      T[x].r = getc(file);
      val = getc(file); // Ignore 1 byte
    }
    waste = (*_imgW)%4;
    if(waste)
      waste = 4-waste;
    for(y=(*_imgH)-1;y>=0;y--) {
      for(x=0;x<*_imgW;x++) {
        val = getc(file);
        _img[0][y][x] = T[val];
      }
      cnt+=fread(buffer,waste,1,file);
    }
  }
  else {
    fprintf(stderr,"%s: fscanbmp_pixelm: error: bmp format with bitdepth=%d not supported\n",caller,bitdepth);
    free(*_img);
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

int fscanjpg_pixelm(FILE *file, pixel ***_img, int *_imgW, int *_imgH, char* caller) {
  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr jerr;
  JSAMPROW row_p[1];
  gray **gimg=NULL;

  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_decompress(&cinfo);
  jpeg_stdio_src(&cinfo, file);
  (void)jpeg_read_header(&cinfo, TRUE);
  (void)jpeg_start_decompress(&cinfo);

  fprintf(stderr,"%s: fscanjpg_pixelm: read image size %dx%d\n",caller,cinfo.output_width,cinfo.output_height);
  if(cinfo.output_width>10000 || cinfo.output_height>10000)
    return EXIT_FAILURE;

  if(malloc_pixelm(cinfo.output_width,cinfo.output_height,_img,0)) {
    fprintf(stderr,"%s: fscanjpg_pixelm: error: unable to reserve memory\n",caller);
    return EXIT_FAILURE;
  }
  if(cinfo.output_components==3) {
    for(row_p[0]=(JSAMPROW)_img[0][0];cinfo.output_scanline<cinfo.output_height;row_p[0]+=3*cinfo.output_width)
      (void)jpeg_read_scanlines(&cinfo,row_p,1);
  }
  else if(cinfo.output_components==1) {
    if(malloc_graym(cinfo.output_width,cinfo.output_height,&gimg,0)) {
      fprintf(stderr,"%s: fscanjpg_pixelm: error: unable to reserve memory\n",caller);
      return EXIT_FAILURE;
    }
    for(row_p[0]=gimg[0];cinfo.output_scanline<cinfo.output_height;row_p[0]+=cinfo.output_width)
      (void)jpeg_read_scanlines(&cinfo,row_p,1);
    graym2pixelm(gimg,cinfo.output_width,cinfo.output_height,*_img);
    free(gimg);
  }
  else {
    fprintf(stderr,"%s: fscanjpg_pixelm: error: jpeg format with %d components is not supported\n",caller,cinfo.output_components);
    free(_img[0]);
    return EXIT_FAILURE;
  }

  *_imgW = cinfo.output_width;
  *_imgH = cinfo.output_height;

  (void)jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);

  return EXIT_SUCCESS;
}

int fscanpng_rgbam(FILE *file, pixel ***_img, gray ***_alph, int *_imgW, int *_imgH, char* caller) {

  png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  png_infop info_ptr = png_create_info_struct(png_ptr);
  if(setjmp(png_jmpbuf(png_ptr))) {
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    return EXIT_FAILURE;
  }

  png_init_io(png_ptr, file);
  png_read_info(png_ptr, info_ptr);
  png_byte color_type = png_get_color_type(png_ptr, info_ptr);
  png_byte bit_depth = png_get_bit_depth(png_ptr, info_ptr);
  *_imgW = png_get_image_width(png_ptr, info_ptr);
  *_imgH = png_get_image_height(png_ptr, info_ptr);

  if(color_type == PNG_COLOR_TYPE_PALETTE)
    png_set_expand(png_ptr);
  if(color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
    png_set_expand(png_ptr);
  if(png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
    png_set_expand(png_ptr);

  if( bit_depth == 16 )
    png_set_strip_16(png_ptr);
  if( color_type == PNG_COLOR_TYPE_GRAY ||
      color_type == PNG_COLOR_TYPE_GRAY_ALPHA )
    png_set_gray_to_rgb(png_ptr);

  double gamma;
  if(png_get_gAMA(png_ptr, info_ptr, &gamma))
    png_set_gamma(png_ptr, 1, gamma);

  png_read_update_info(png_ptr, info_ptr);

  gray **tmp;

  int err = 0;
  err += malloc_pixelm(*_imgW,*_imgH,_img,0);
  *_alph = NULL;
  if( color_type & PNG_COLOR_MASK_ALPHA ) {
    err += malloc_graym(*_imgW*4,1,&tmp,0);
    err += malloc_graym(*_imgW,*_imgH,_alph,0);
  }
  if(err) {
    fprintf(stderr,"%s: fscanpng_pixelm: error: unable to reserve memory\n",caller);
    return EXIT_FAILURE;
  }

  int y;
  if( color_type & PNG_COLOR_MASK_ALPHA ) {
    for(y=0;y<*_imgH;y++) {
      png_read_row(png_ptr, (png_bytep) tmp[0], NULL);
      pixel *imgy = _img[0][y];
      gray *alphy = _alph[0][y];
      int x,n;
      for(x=0,n=0;x<*_imgW;x++,n+=4) {
        imgy[x].r = tmp[0][n];
        imgy[x].g = tmp[0][n+1];
        imgy[x].b = tmp[0][n+2];
        alphy[x]  = tmp[0][n+3];
      }
    }
    free(tmp);
  }
  else {
    unsigned char *ptr=(unsigned char*)_img[0][0];
    for(y=*_imgH;y--;) {
      png_read_row(png_ptr, (png_bytep) ptr, NULL);
      ptr += *_imgW*3;
    }
  }

  png_read_end(png_ptr, NULL);
  png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

  return EXIT_SUCCESS;
}

int fscanpng_pixelm(FILE *file, pixel ***_img, int *_imgW, int *_imgH, char* caller) {

  png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  png_infop info_ptr = png_create_info_struct(png_ptr);
  if(setjmp(png_jmpbuf(png_ptr))) {
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    return EXIT_FAILURE;
  }

  png_init_io(png_ptr, file);
  png_read_info(png_ptr, info_ptr);
  png_byte color_type = png_get_color_type(png_ptr, info_ptr);
  png_byte bit_depth = png_get_bit_depth(png_ptr, info_ptr);
  //int channels = (int)png_get_channels(png_ptr, info_ptr);
  *_imgW = png_get_image_width(png_ptr, info_ptr);
  *_imgH = png_get_image_height(png_ptr, info_ptr);

  if(color_type == PNG_COLOR_TYPE_PALETTE)
    png_set_expand(png_ptr);
  if(color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
    png_set_expand(png_ptr);
  if(png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
    png_set_expand(png_ptr);

  if( bit_depth == 16 )
    png_set_strip_16(png_ptr);
  if( color_type == PNG_COLOR_TYPE_GRAY ||
      color_type == PNG_COLOR_TYPE_GRAY_ALPHA )
    png_set_gray_to_rgb(png_ptr);

  double gamma;
  if(png_get_gAMA(png_ptr, info_ptr, &gamma))
    png_set_gamma(png_ptr, 1, gamma);

  png_read_update_info(png_ptr, info_ptr);

  if(malloc_pixelm(*_imgW,*_imgH,_img,0)) {
    fprintf(stderr,"%s: fscanpng_pixelm: error: unable to reserve memory\n",caller);
    return EXIT_FAILURE;
  }

  unsigned char *ptr;
  int y;
  //if(channels==3 && color_type==PNG_COLOR_TYPE_RGB) {
    ptr=(unsigned char*)_img[0][0];
    for(y=*_imgH;y--;) {
      png_read_row(png_ptr, (png_bytep) ptr, NULL);
      ptr += *_imgW*3;
    }
  //}
  //else if(channels==1 && color_type==PNG_COLOR_TYPE_GRAY) {
  //  gray** gimg=NULL;
  //  if(malloc_graym(*_imgW,*_imgH,&gimg,0)) {
  //    fprintf(stderr,"%s: fscanpng_pixelm: error: unable to reserve memory\n",caller);
  //    return EXIT_FAILURE;
  //  }
  //  ptr=gimg[0];
  //  for(y=*_imgH;y--;) {
  //    png_read_row(png_ptr, (png_bytep) ptr, NULL);
  //    ptr += *_imgW;
  //  }
  //  graym2pixelm(gimg,*_imgW,*_imgH,*_img);
  //  free(gimg);
  //}
  //else {
  //  fprintf(stderr,"%s: fscanpng_pixelm: error: only RGB and grayscale PNG images supported\n",caller);
  //  return EXIT_FAILURE;
  //}

  png_read_end(png_ptr, NULL);
  png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

  return EXIT_SUCCESS;
}

int fscantif_pixelm(FILE *file, pixel ***_img, int *_imgW, int *_imgH, char* caller) {
  char *stmp = tmpnam(NULL);
  FILE *tmp = fopen(stmp,"wb+");
  while(!feof(file))
    fputc(fgetc(file), tmp);
  fflush(tmp);
  fclose(tmp);

  TIFF* tif = TIFFOpen(stmp, "r");
  if(tif) {
    uint32 w, h;
    uint32* raster;

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);

    *_imgW = w;
    *_imgH = h;

    if(malloc_pixelm(w,h,_img,0) ||
       (raster = (uint32*)_TIFFmalloc(w*h*sizeof(uint32)))==NULL) {
      fprintf(stderr,"%s: fscantif_pixelm: error: unable to reserve memory\n",caller);
      return EXIT_FAILURE;
    }

    //if(TIFFReadRGBAImage(tif, w, h, raster, 0)) {
    if(TIFFReadRGBAImageOriented(tif, w, h, raster, ORIENTATION_TOPLEFT, 0)) {
      int y;
      for(y=h-1;y>=0;y--) {
        //pixel *imgy = _img[0][h-y-1];
        pixel *imgy = _img[0][y];
        int x;
        for(x=w-1;x>=0;x--) {
          imgy[x].r = 0xFF&(raster[w*y+x]);
          imgy[x].g = 0xFF&(raster[w*y+x]>>8);
          imgy[x].b = 0xFF&(raster[w*y+x]>>16);
	}
      }
      _TIFFfree(raster);
    }
    TIFFClose(tif);
  }

  else {
    fprintf(stderr,"%s: fscantif_pixelm: error: unable to read TIFF image\n",caller);
    return EXIT_FAILURE;
  }

  unlink(stmp);

  return EXIT_SUCCESS;
}

int writeimg_graym(char* fname, gray** img, int imgW, int imgH, char* caller) {
  int n;
  char *p,ext[EXTMAX+1];
  FILE *file;
  pixel **cimg;

  if((p=strrchr(fname,'.'))==NULL) {
    fprintf(stderr,"%s: writeimg_graym: error: file %s does not have a valid extension.\n",caller,fname);
    return EXIT_FAILURE;
  }
  if((file=fopen(fname,"wb"))==NULL) {
    fprintf(stderr,"%s: writeimg_graym: error: unable to open file %s for writing.\n",caller,fname);
    return EXIT_FAILURE;
  }
  strncpy(ext,p+1,EXTMAX);
  for(n=0;n<EXTMAX;n++)
    if(ext[n]>='A' && ext[n]<='Z')
      ext[n]+=32;
  ext[n] = '\0';
  if(!strncmp(ext,"pgm",3) || !strncmp(ext,"png",3) || !strncmp(ext,"tif",3) || !strncmp(ext,"tiff",4) || !strncmp(ext,"jpg",3) || !strncmp(ext,"jpeg",4)) {
    if(!strncmp(ext,"pgm",3))
      fprintpgm_graym(file,img,imgW,imgH,IMGIO_GRAYMAX,DEFAULT_PGM_FORMAT);
    else if(!strncmp(ext,"png",3))
      fprintpng_graym(file,img,NULL,imgW,imgH);
    else if(!strncmp(ext,"tif",3) || !strncmp(ext,"tiff",4))
      fprinttif_graym(file,img,imgW,imgH);
    else
      fprintjpg_graym(file,img,imgW,imgH,DEFAULT_JPEG_QUALITY);
  }
  else if(!strncmp(ext,"ppm",3) || !strncmp(ext,"bmp",3) || !strncmp(ext,"dib",3)) {
    if(malloc_pixelm(imgW,imgH,&cimg,0)) {
      fclose(file);
      return EXIT_FAILURE;
    }
    graym2pixelm(img,imgW,imgH,cimg);
    if(!strncmp(ext,"ppm",3))
      fprintppm_pixelm(file,cimg,imgW,imgH,IMGIO_GRAYMAX,DEFAULT_PPM_FORMAT);
    else
      fprintbmp_pixelm(file,cimg,imgW,imgH);
    free(cimg);
  }
  else {
    fprintf(stderr,"%s: writeimg_pixelm: error: image format %s not supported.\n",caller,p);
    return EXIT_FAILURE;
  }
  fclose(file);

  return EXIT_SUCCESS;
}

int writeimg_pixelm(char* fname, pixel** img, int imgW, int imgH, char* caller) {
  int n;
  char *p,ext[EXTMAX+1];
  FILE *file;
  gray **gimg;

  if((p=strrchr(fname,'.'))==NULL) {
    fprintf(stderr,"%s: writeimg_pixelm: error: file %s does not have a valid extension.\n",caller,fname);
    return EXIT_FAILURE;
  }
  if((file=fopen(fname,"wb"))==NULL) {
    fprintf(stderr,"%s: writeimg_pixelm: error: unable to open file %s for writing.\n",caller,fname);
    return EXIT_FAILURE;
  }
  strncpy(ext,p+1,EXTMAX);
  for(n=0;n<EXTMAX;n++)
    if(ext[n]>='A' && ext[n]<='Z')
      ext[n]+=32;
  ext[n] = '\0';
  if(!strncmp(ext,"ppm",3))
    fprintppm_pixelm(file,img,imgW,imgH,IMGIO_GRAYMAX,DEFAULT_PPM_FORMAT);
  else if(!strncmp(ext,"jpg",3) || !strncmp(ext,"jpeg",4))
    fprintjpg_pixelm(file,img,imgW,imgH,DEFAULT_JPEG_QUALITY);
  else if(!strncmp(ext,"tif",3) || !strncmp(ext,"tiff",4))
    fprinttif_pixelm(file,img,imgW,imgH);
  else if(!strncmp(ext,"bmp",3) || !strncmp(ext,"dib",3))
    fprintbmp_pixelm(file,img,imgW,imgH);
  else if(!strncmp(ext,"png",3))
    fprintpng_pixelm(file,img,NULL,imgW,imgH);
  else if(!strncmp(ext,"pgm",3)) {
    if(malloc_graym(imgW,imgH,&gimg,0)) {
      fclose(file);
      return EXIT_FAILURE;
    }
    rgbm2graym(img,imgW,imgH,gimg);
    fprintpgm_graym(file,gimg,imgW,imgH,IMGIO_GRAYMAX,DEFAULT_PGM_FORMAT);
    free(gimg);
  }
  else {
    fprintf(stderr,"%s: writeimg_pixelm: error: image format %s not supported.\n",caller,p);
    return EXIT_FAILURE;
  }
  fclose(file);

  return EXIT_SUCCESS;
}

int fprintpgm_graym(FILE* file, gray** img, int imgW, int imgH, gray maxval, char format) {
  int x,y,n;
  int cnt=0;

  if(!(format=='2'||format=='5'))
    return EXIT_FAILURE;

  fprintf(file,"P%c\n%d %d\n%d\n",format,imgW,imgH,maxval);
  if(format=='2') {
    for(y=0,n=4;y<imgH;y++)
      for(x=0;x<imgW;x++) {
        if(n>LINEMAX || (x==imgW-1 && y==imgH-1) ) {
          fprintf(file,"%.3d\n",(int)img[y][x]);
          n = 4;
        }
        else {
          fprintf(file,"%.3d ",(int)img[y][x]);
          n += 4;
        }
      }
  }
  else if(format=='5')
    cnt+=fwrite(img[0],sizeof(gray),imgW*imgH,file);

  return EXIT_SUCCESS;
}

int fprintppm_pixelm(FILE* file, pixel** img, int imgW, int imgH, gray maxval, char format) {
  int x,y,n;
  int cnt=0;

  if(!(format=='3'||format=='6'))
    return EXIT_FAILURE;

  fprintf(file,"P%c\n%d %d\n%d\n",format,imgW,imgH,maxval);
  if(format=='3') {
    for(y=0,n=12;y<imgH;y++)
      for(x=0;x<imgW;x++) {
        if(n>LINEMAX || (x==imgW-1 && y==imgH-1) ) {
          fprintf(file,"%.3d %.3d %.3d\n",(int)img[y][x].r,(int)img[y][x].g,(int)img[y][x].b);
          n = 12;
        }
        else {
          fprintf(file,"%.3d %.3d %.3d ",(int)img[y][x].r,(int)img[y][x].g,(int)img[y][x].b);
          n += 12;
        }
      }
  }
  else if(format=='6')
    cnt+=fwrite(img[0],sizeof(pixel),imgW*imgH,file);

  return EXIT_SUCCESS;
}

int fprintbmp_pixelm(FILE* file, pixel** img, int imgW, int imgH) {
  int x,y,aux,waste;
  short saux;
  int cnt=0;

  fprintf(file,"BM");        // Header
  aux = 54+imgW*imgH*3;
  cnt+=fwrite(&aux,4,1,file);     // Size of file
  aux = 0;
  cnt+=fwrite(&aux,4,1,file);     // Always zero
  aux = 54;
  cnt+=fwrite(&aux,4,1,file);     // Offset to bitmap
  aux = 40;
  cnt+=fwrite(&aux,4,1,file);     // Size of info header
  cnt+=fwrite(&imgW,4,1,file);    // Width of image
  cnt+=fwrite(&imgH,4,1,file);    // Height of image
  saux = 1;
  cnt+=fwrite(&saux,2,1,file);    // Number of planes
  saux = 24;
  cnt+=fwrite(&saux,2,1,file);    // Bit depth
  aux = 0;
  cnt+=fwrite(&aux,4,1,file);     // Compression
  cnt+=fwrite(&aux,4,1,file);     // Size of image data
  cnt+=fwrite(&aux,4,1,file);     // Pixels per meter horizontal
  cnt+=fwrite(&aux,4,1,file);     // Pixels per meter vertical
  cnt+=fwrite(&aux,4,1,file);     // Number of colors
  cnt+=fwrite(&aux,4,1,file);     // Number of important colors
  waste = (imgW*3)%4;
  if(waste)
    waste = 4-waste;
  for(y=imgH-1;y>=0;y--) {
    for(x=0;x<imgW;x++)
    {
      cnt+=fwrite(&(img[y][x].b),1,1,file);   // Blue
      cnt+=fwrite(&(img[y][x].g),1,1,file);   // Green
      cnt+=fwrite(&(img[y][x].r),1,1,file);   // Red
    }
    cnt+=fwrite(&aux,waste,1,file);
  }

  return EXIT_SUCCESS;
}

int fprintjpg_graym(FILE *file, gray **img, int imgW, int imgH, int quality) {
  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;

  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress(&cinfo);
  jpeg_stdio_dest(&cinfo,file);

  cinfo.image_width = imgW;
  cinfo.image_height = imgH;
  cinfo.input_components = 1;
  cinfo.in_color_space = JCS_GRAYSCALE;

  jpeg_set_defaults(&cinfo);
  jpeg_set_quality(&cinfo,quality,TRUE);
  jpeg_start_compress(&cinfo,TRUE);

  jpeg_write_scanlines(&cinfo,(JSAMPROW*)img,imgH);

  (void)jpeg_finish_compress(&cinfo);
  jpeg_destroy_compress(&cinfo);

  return EXIT_SUCCESS;
}

int fprintjpg_pixelm(FILE *file, pixel **img, int imgW, int imgH, int quality) {
  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;

  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress(&cinfo);
  jpeg_stdio_dest(&cinfo,file);

  cinfo.image_width = imgW;
  cinfo.image_height = imgH;
  cinfo.input_components = 3;
  cinfo.in_color_space = JCS_RGB;

  jpeg_set_defaults(&cinfo);
  jpeg_set_quality(&cinfo,quality,TRUE);
  jpeg_start_compress(&cinfo,TRUE);

  jpeg_write_scanlines(&cinfo,(JSAMPROW*)img,imgH);

  (void)jpeg_finish_compress(&cinfo);
  jpeg_destroy_compress(&cinfo);

  return EXIT_SUCCESS;
}

int fprintpng_graym(FILE *file, gray **img, gray **alph, int imgW, int imgH) {
  png_structp png_ptr;
  png_infop info_ptr;

  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if(png_ptr == NULL)
    return EXIT_FAILURE;

  info_ptr = png_create_info_struct(png_ptr);
  if(info_ptr == NULL) {
    png_destroy_write_struct(&png_ptr,  (png_infopp)NULL);
    return EXIT_FAILURE;
  }

  if(setjmp(png_jmpbuf(png_ptr))) {
    //png_destroy_write_struct(&png_ptr,  (png_infopp)NULL);
    png_destroy_write_struct(&png_ptr,  &info_ptr);
    return EXIT_FAILURE;
  }

  png_init_io(png_ptr, file);

  if(alph==NULL)
    png_set_IHDR(png_ptr, info_ptr, imgW, imgH, 8, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
  else
    png_set_IHDR(png_ptr, info_ptr, imgW, imgH, 8, PNG_COLOR_TYPE_GRAY_ALPHA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  png_set_compression_level(png_ptr, 6);

  png_write_info(png_ptr, info_ptr);

  if(alph==NULL)
    png_write_image(png_ptr, (png_bytepp)img);
  else {
    gray **timg;
    int err = malloc_graym(2*imgW,imgH,&timg,0);
    if(err!=EXIT_SUCCESS)
      return err;

    int y;
    for(y=0;y<imgH;y++) {
      gray *imgy = img[y];
      gray *alphy = alph[y];
      gray *timgy = timg[y];
      int x,n;
      for(x=0,n=0;x<imgW;x++,n+=2) {
        timgy[n] = imgy[x];
        timgy[n+1] = alphy[x];
      }
    }

    png_write_image(png_ptr, (png_bytepp)timg);
    free(timg);
  }

  png_write_end(png_ptr, info_ptr);

  //free(info_ptr->palette);

  //png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
  png_destroy_write_struct(&png_ptr, &info_ptr);

  return EXIT_SUCCESS;
}

int fprintpng_pixelm(FILE *file, pixel **img, gray **alph, int imgW, int imgH) {
  png_structp png_ptr;
  png_infop info_ptr;

  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if(png_ptr == NULL)
    return EXIT_FAILURE;

  info_ptr = png_create_info_struct(png_ptr);
  if(info_ptr == NULL) {
    png_destroy_write_struct(&png_ptr,  (png_infopp)NULL);
    return EXIT_FAILURE;
  }

  if(setjmp(png_jmpbuf(png_ptr))) {
    //png_destroy_write_struct(&png_ptr,  (png_infopp)NULL);
    png_destroy_write_struct(&png_ptr,  &info_ptr);
    return EXIT_FAILURE;
  }

  png_init_io(png_ptr, file);

  if(alph==NULL)
    png_set_IHDR(png_ptr, info_ptr, imgW, imgH, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
  else
    png_set_IHDR(png_ptr, info_ptr, imgW, imgH, 8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  png_set_compression_level(png_ptr, 6);

  png_write_info(png_ptr, info_ptr);

  if(alph==NULL)
    png_write_image(png_ptr, (png_bytepp)img);
  else {
    gray **timg;
    int err = malloc_graym(4*imgW,imgH,&timg,0);
    if(err!=EXIT_SUCCESS)
      return err;

    int y;
    for(y=0;y<imgH;y++) {
      pixel *imgy = img[y];
      gray *alphy = alph[y];
      gray *timgy = timg[y];
      int x,n;
      for(x=0,n=0;x<imgW;x++,n+=4) {
        timgy[n] = imgy[x].r;
        timgy[n+1] = imgy[x].g;
        timgy[n+2] = imgy[x].b;
        timgy[n+3] = alphy[x];
      }
    }

    png_write_image(png_ptr, (png_bytepp)timg);
    free(timg);
  }

  png_write_end(png_ptr, info_ptr);

  //free(info_ptr->palette);

  //png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
  png_destroy_write_struct(&png_ptr, &info_ptr);

  return EXIT_SUCCESS;
}

int fprinttif_bitm(FILE *file, gray **img, int imgW, int imgH) {
  char *stmp = tmpnam(NULL);
  TIFF *tif = TIFFOpen(stmp,"w");

  TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imgW);
  TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imgH);
  TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
  TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 1);
  TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_LZW);
  TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
  TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
  TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISWHITE);
  TIFFSetField(tif, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);

  gray *raster;
  if(mem(((int)ceil((float)imgW/8))*imgH*sizeof(gray),1,(char**)(&raster)))
    return EXIT_FAILURE;

  int n = 0;
  int y;
  for(y=0;y<imgH;y++) {
    int x;
    gray *imgy = img[y];
    for(x=0;x<imgW;x+=8) {
      int tmp = 0;
      int k;
      for(k=0;(k<8)&&(x+k<imgW);k++)
        tmp = tmp | ((0x01<<(7-k))&((int)~imgy[x+k]));
      raster[n] = tmp;
      n++;
    }
  }

  if(TIFFWriteEncodedStrip(tif, 0, raster, (int)ceil((float)imgW/8)*imgH) == 0)
    return EXIT_FAILURE;

  TIFFClose(tif);
  free(raster);

  FILE *tmp = fopen(stmp,"rb");
  while(!feof(tmp))
    fputc(fgetc(tmp), file);
  fclose(tmp);

  unlink(stmp);

  return EXIT_SUCCESS;
}

int fprinttif_graym(FILE *file, gray **img, int imgW, int imgH) {
  char *stmp = tmpnam(NULL);
  TIFF *tif = TIFFOpen(stmp,"w");

  TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imgW);
  TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imgH);
  TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
  TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);
  TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_DEFLATE);
  TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
  TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
  TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);

  if(TIFFWriteEncodedStrip(tif, 0, img[0], imgW*imgH) == 0)
    return EXIT_FAILURE;

  TIFFClose(tif);

  FILE *tmp = fopen(stmp,"rb");
  while(!feof(tmp))
    fputc(fgetc(tmp), file);
  fclose(tmp);

  unlink(stmp);

  return EXIT_SUCCESS;
}

int fprinttif_pixelm(FILE *file, pixel **img, int imgW, int imgH) {
  char *stmp = tmpnam(NULL);
  TIFF *tif = TIFFOpen(stmp,"w");

  TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imgW);
  TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imgH);
  TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 3);
  TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);
  TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_DEFLATE);
  TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
  TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
  TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);

  gray *raster;
  if(mem(3*imgW*imgH*sizeof(gray),0,(char**)(&raster)))
    return EXIT_FAILURE;

  int n;
  for(n=imgW*imgH-1;n>=0;n--) {
    raster[3*n]   = img[0][n].r;
    raster[3*n+1] = img[0][n].g;
    raster[3*n+2] = img[0][n].b;
  }

  if(TIFFWriteEncodedStrip(tif, 0, raster, imgW*imgH*3) == 0)
    return EXIT_FAILURE;

  TIFFClose(tif);
  free(raster);

  FILE *tmp = fopen(stmp,"rb");
  while(!feof(tmp))
    fputc(fgetc(tmp), file);
  fclose(tmp);

  unlink(stmp);

  return EXIT_SUCCESS;
}
