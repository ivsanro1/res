#!/bin/bash
D=`pwd`
NAME=${0##*/}
if [ $# -ne 1 ]; then
  echo "Usage: $NAME <Dir-BIN>" 1>&2
  exit
fi

BIN=$1



for f in *SRC; do cd $f; make; cd ..;  mv $f/${f/SRC/} ${BIN};  done
