/*
    Copyright (C) 2013 Moisés Pastor 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define UMBRAL_VERTICAL 1

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <values.h>
#include <unistd.h>
#include <imageClass.h>


class slantClass: public imageClass {
  char method;
  bool global;
  float threshold;
  int separacion_tramos, longMinTramo;
  float MVPV(int inicio_tramo, int fin_tramo);
  float IDIAP_slant(int inicio_tramo, int fin_tramo);
  float Javi_slant(int inicio_tramo, int fin_tramo);
  void shear(imageClass * fixedImage, int inicio, int fin, float angle);
  float var_VProjection(int *VPr,int cols);
  int deteccionTramos( int ** inicio_tramo, int ** fin_tramo);
  int * getV_Projection();
  int ** get_projections(int inicio_tramo, int fin_tramo);
public:
  unsigned int ProuNegre;
  slantClass(char method='M',bool global=false, float threshold=1,int separacion_tramos=-1, int longMinTramo=10)
  {
    this->method = method; 
    this->global = global; 
    this->threshold = threshold;
    this->separacion_tramos = separacion_tramos;
    this->longMinTramo = longMinTramo;
  }
  imageClass * deslant();

};


void slantClass::shear(imageClass *  fixedImage, int inicio, int fin, float angle) {
  int r,c,despl=0,rows_2;
  double shearfac=tan((double)angle);
  static int inicial_desp=0;
  float rest_desp=0;
  rows_2=rows/2;

  if ((inicio - fabs(rows_2*shearfac)) < 0)
    inicial_desp=(int)fabs(rows_2*shearfac);


  // comprovar si se'n va per l'esquerra
  for (r=0;r<=rows_2;r++){
    despl=(int)((rows_2 - r)*shearfac)- inicial_desp;
    rest_desp=((((rows_2 - r)*shearfac)- inicial_desp) -despl);
    for (c=inicio;c<fin;c++){
      if (c-despl < 0 || c+1 >= fixedImage->cols) cerr << "columna fuera de rango "<<endl;
      if (rest_desp <0){
	fixedImage->image[r][c-despl]=(gray)(image[r][c]*(-rest_desp)+image[r][c+1]*(1+rest_desp));
      }else{
	fixedImage->image[r][c-despl]=(gray)(image[r][c+1]*rest_desp+image[r][c]*(1-rest_desp));
      }
    }
  }
  for (r=rows_2+1;r<rows;r++){
    despl=(int)((r - rows_2)*shearfac) + inicial_desp;
    rest_desp=(((r - rows_2)*shearfac) + inicial_desp -despl);
    for (c=inicio;c<fin;c++){
      if (c+despl >= fixedImage->cols || c+1 >= fixedImage->cols)  cerr << "2 columna fuera de rango "<<endl;
      if (rest_desp <0){
	fixedImage->image[r][c+despl]=(gray)(image[r][c+1]*(-rest_desp)+image[r][c]*(1+rest_desp));
      } else{
	
	fixedImage->image[r][c+despl]=(gray)(image[r][c]*rest_desp+image[r][c+1]*(1-rest_desp));
	
      }
    }
  }
}

inline float slantClass::var_VProjection(int *VPr,int cols) {
  int i;
  float sum=0, sum2=0,var,mean;
  int *V_Proj=VPr;
  
  for (i=0;i<cols;VPr++,i++){
    sum+=(float)(*VPr);
    sum2+= (float)(*VPr)*(*VPr);
  }
  mean=sum/cols;
  var=(sum2 - 2 * mean * sum + sum2)/cols;

  if (verbosity == 3){
    fprintf(stderr,"#Vertical Projection\n");
    for (i=0;i<cols;i++) 
      fprintf(stderr,"%d %d\n",i, V_Proj[i]); 
  }

  return sqrt(var);
} 

int * slantClass::getV_Projection(){ 
    /* Calculamos la proyeccion vertical */
    int * histver;
    int i,j;
    histver= new int [cols];
    for (j=0; j < cols; j++)
        histver[j]=0;
    
    for (j=0; j < cols; j++) {
      for (i=0; i < rows ; i++) 
	if (image[i][j] < this->ProuNegre){ 
	  histver[j]++;
	}
      
    } 
    return histver;
}


int slantClass::deteccionTramos( int ** inicio_tramo, int ** fin_tramo){
    int  n_tramos=0, i,hueco;
    int *histver;
    
    *inicio_tramo=new int [cols];
    *fin_tramo= new int[cols];
    
    /* Calculamos la proyeccion vertical */
    histver=getV_Projection();
   
    /* Calculamos el tama�o medio de los huecos */
    if (separacion_tramos==-1)
      separacion_tramos=tamanyo_medio_huecos(histver, cols);
    //cerr << "separacion " << separacion_tramos << endl;
    /* Dividimos la imagen en tramos contiguos*/
    n_tramos=0;
   /* quitamos posibles blancos iniciales */
    i=0;
    while (i < cols && histver[i] < UMBRAL_VERTICAL)  i++;
    if (i==cols) 
        return (-1);

    /*Dividimos en tramos */
    (*inicio_tramo)[n_tramos]=i; 
    while(1) {
        for (; i<cols && histver[i]>=UMBRAL_VERTICAL; i++);
        if (i==cols) { 
            (*fin_tramo)[n_tramos]=cols-1; 
            break;
        }
        (*fin_tramo)[n_tramos]=i; 
        for (hueco=0; i<cols && histver[i]<UMBRAL_VERTICAL; i++, hueco++);
        if (i==cols) { 
            (*fin_tramo)[n_tramos]=cols-1; 
            break;
       }
        if (hueco>=separacion_tramos) {
            n_tramos++;
            (*inicio_tramo)[n_tramos]=i;
        }
    }
    delete [] histver;
    return(n_tramos+1);
    
}


int ** slantClass::get_projections(int inicio_tramo, int fin_tramo){
  int row,col,despl_inicial,dim_prj;
  int a,i;
  int desp[91];
  int ** VProj;
  gray * pcol;
  
  despl_inicial=rows - inicio_tramo;
  dim_prj=(fin_tramo-inicio_tramo)+2*rows ;

  /* Pedimos memoria para las proyecciones */
  VProj= new int*[91];
  for (i=0;i<=90;i++)
    VProj[i]= new int [dim_prj];

  /* Inicializamos las proyecciones */
  for (i=0;i<=90;i++)
    for (col=0; col < dim_prj; col++)
      VProj[i][col]=0;


  /* Calculamos las proyecciones */
  for (row=0;row<rows;row++){
    for (a=0;a<=90;a++)
      desp[a]=(int)((rows-row)*tan((float)(a-45)*M_PI/180.0)); /* todos los 
								  despl para 
								  cada fila */
    for (col=inicio_tramo,pcol=&image[row][inicio_tramo] ; col < fin_tramo;pcol++, col++) 
      if (*pcol < ProuNegre)         /* lo tenemos en cuenta */
	for (a=0;a<=90;a++)           /* si es suficientemente negro */
	  if ((despl_inicial+col+desp[a]) >= 0 && (despl_inicial+col+desp[a]) < dim_prj)
	    /* VProj[a+30][despl_inicial+col+desp[a+30]]+=image[row][col]; */
	    VProj[a][despl_inicial+col+desp[a]]++;
  }
  return VProj;
}

////////////////////////////////////////////////////////////////////////////
/// funciones objetivo 
////////////////////////////////////////////////////////////////////////////
int CalcLongPerfil(int *projection, int rows) {
  int    i;
  double l,lperf;
  lperf=0;
  for (i=1; i<rows; i++) {
    l=projection[i]-projection[i-1];
    lperf+=l*l;
  }
  return(lperf);
}
////////////////////////////////////////////////////////////////////////////
float slantClass::Javi_slant(int inicio_tramo, int fin_tramo){
  int dim_prj; 
  int a,best_a=0,sum,max_sum,i;
  int ** VProj;
 
  VProj=get_projections(inicio_tramo,fin_tramo);
  // dim_prj=(fin_tramo-inicio_tramo)*4;
  dim_prj=(fin_tramo-inicio_tramo)+2*rows ;
  max_sum=0;

  if (verbosity==2)
    fprintf(stderr,"#contorn length distribution\n");

  for (a=0;a<=90;a++) {
    sum=0;
    sum = CalcLongPerfil(VProj[a], dim_prj);
    if (verbosity==2)
      fprintf(stderr,"%i %i\n",a-45,sum);
    
    if (sum > max_sum) {	
      max_sum=sum;
      best_a=a-45;
    }
  }
 
  /* Liberamos la memoria de las proyectiones */
  for (i=0;i<=90;i++)
    delete [] VProj[i];

  delete [] VProj;
  
  return(-best_a);
}

////////////////////////////////////////////////////////////////////////////
float slantClass::IDIAP_slant(int inicio_tramo, int fin_tramo){
  int row,col,c,dim_prj; 
  int a,best_a,sum,despl_inicial,max_sum,i,dist;
  int desp[91];
  int ** VProj,**MaxCol,**MinCol;
  gray * pcol;
  
  //despl_inicial=(fin_tramo-inicio_tramo)*1.5 - inicio_tramo;
  despl_inicial=rows - inicio_tramo;
  //dim_prj=(fin_tramo-inicio_tramo)*4;
  dim_prj=(fin_tramo-inicio_tramo)+2*rows ;

  /* Pedimos memoria para las proyecciones */
  VProj=new int*[91];
  MaxCol=new int*[91];
  MinCol=new int*[91];

  for (i=0;i<=90;i++){
    VProj[i]=new int [dim_prj];
    MaxCol[i]=new int [dim_prj];
    MinCol[i]=new int [dim_prj];
  }
  /* Inicializamos las proyecciones */
  for (i=0;i<=90;i++)
    for (col=0; col < dim_prj; col++){
      VProj[i][col]=0;
      MaxCol[i][col]=-1;
      MinCol[i][col]=cols+1;
    }
  
  /* Calculamos las proyecciones */
  for (row=0;row<rows;row++){
    for (a=0;a<=90;a++)
      desp[a]=(int)((rows-row)*tan((float)(a-45)*M_PI/180.0)); /* todos los 
								  despl para 
								  cada fila */
    for (col=inicio_tramo,pcol=&image[row][inicio_tramo] ; col < fin_tramo;pcol++, col++) {
      if (*pcol < ProuNegre)         /* lo tenemos en cuenta */
	for (a=0;a<=90;a++)           /* si es suficientemente negro */
	  if ((despl_inicial+col+desp[a]) >= 0 && (despl_inicial+col+desp[a]) < dim_prj){
	    /* VProj[a+30][despl_inicial+col+desp[a+30]]+=image[row][col]; */
	    VProj[a][despl_inicial+col+desp[a]]++;
	    if (row > MaxCol[a][despl_inicial+col+desp[a]]) MaxCol[a][despl_inicial+col+desp[a]]= row;
	    if (row < MinCol[a][despl_inicial+col+desp[a]]) MinCol[a][despl_inicial+col+desp[a]]= row;
	  }
    }
  }

  /* verbosidad de las proyecciones */
  if (verbosity == 3){
    fprintf(stderr,"#Vertical Projection\n");
    fprintf(stderr,"# ");
    for (a=0;a<=90;a++) fprintf(stderr,"%d ",a-45);
    fprintf(stderr,"\n");
    for (c=0;c<dim_prj;c++) {
      fprintf(stderr,"%d ",c);
      for (a=0;a<=90;a++)
	fprintf(stderr,"%d ",VProj[a][c]);
      fprintf(stderr,"\n");
    }
    fprintf(stderr,"# -------------------------\n");
  }
  
  max_sum=0;
  best_a=INT_MIN;
  
  if (verbosity==2)
    fprintf(stderr,"#Idiap scores distribution\n");
  for (a=0;a<=90;a++) {
    sum=0;
    for (col=0; col < dim_prj; col++) {
      dist=MaxCol[a][col]-MinCol[a][col] + 1;
      //fprintf(stderr,"%f \n",VProj[a][col]/(float)dist);
      if (VProj[a][col] > 0 && dist > 0 && ((VProj[a][col]/(float)dist) >= threshold))
	sum+=VProj[a][col]*VProj[a][col];
    }
    if (sum > max_sum) {	
      max_sum=sum;
      best_a=a-45;
    }
    if (verbosity==2)
      fprintf(stderr,"%i %i\n",a-45,sum);
  }
  //  if (best_a==-10000) best_a=0;
  /* Liberamos la memoria de las proyectiones */
  for (i=0;i<=90;i++){
    delete [] VProj[i];
    delete [] MaxCol[i];
    delete [] MinCol[i];
  }
  delete [] VProj;
  delete [] MaxCol;
  delete [] MinCol;

  return(-best_a);
}

////////////////////////////////////////////////////////////////////////////

float slantClass::MVPV( int inicio_tramo, int fin_tramo){
  float var_max,var[91],sum,cont;
  int a;//, a_max;
  int ** VProj;
  int dim_prj;
  
  //dim_prj=(fin_tramo-inicio_tramo)*4;
  dim_prj=(fin_tramo-inicio_tramo)+2*rows ;
  VProj=get_projections(inicio_tramo, fin_tramo);

  /* Calculamos las varianzas de las proyecciones*/
  /* y nos quedamos con el angulo que da el maximo */
   var_max=0;
 
  for (a=-44;a<=45;a++){
    var[a+45]=var_VProjection(VProj[a+45],dim_prj);
    //    sum+=var[a+45]; 
    if (var_max < var[a+45]){
      var_max=var[a+45];
      //  a_max=a;
    }
  }
  if (verbosity == 2)
    for (a=-45;a<=45;a++)
      fprintf(stderr,"%d %f\n",a,var[a+45]);
  /* calculamos la media ponderada de todas las varianzas (centro de masas)
     que esten por debajo de un procentaje (threshold) del maximo */
  
  sum=0;
  cont=0;
  for (a=-45;a<=45;a++){
    if (var[a+45] >= threshold*var_max){
      sum+=var[a+45] * a;
      cont+=var[a+45];      
    }
  }  
  /* Liberamos la memoria de las proyectiones */
  for (a=0;a<=90;a++)
    delete [] VProj[a];
  delete [] VProj;
  
  return(-sum/cont);
}

imageClass * slantClass::deslant(){

  int *inicio_tramo,*fin_tramo, n_tramos, tramo;
  float angulo;

  if (verbosity == 1)
    cerr << "# slant: Method " << method << endl;
  
  
  
  /***************************************
   * Deteccion de tramos 
   ***************************************/
  if (global){ /* solo un tramo con toda la imagen */
    inicio_tramo= new int;
    fin_tramo= new int;
    n_tramos=1;
    inicio_tramo[0]=0;
    fin_tramo[0]=cols-1;
  } else {
    n_tramos=deteccionTramos(&inicio_tramo,&fin_tramo);
    if (n_tramos == -1){
      cerr << "#slant: Tramos detectados: "<< n_tramos << endl;
      return new imageClass(*this);
    }  
  }

  imageClass * fixedImage= new imageClass(rows, cols+rows,maxval);
  /**************************************
   * Procesado de los tramos
   **************************************/ 
  if (verbosity==1){
    cerr << "#slant: ------- " << n_tramos << " tramos encontrados -------"<< endl;
    cerr << "#tamanyo medio hueco "<<separacion_tramos<< endl;

  }
 
  for (tramo=0;tramo<n_tramos;tramo++) {
    if (fin_tramo[tramo]-inicio_tramo[tramo]>longMinTramo) {
      if (verbosity)
	fprintf(stderr,"#slant: Tramo %d inicio: %d  fin: %d\n",tramo,inicio_tramo[tramo],fin_tramo[tramo]);
      
      /*****************************************
       * Calculo del angulo por maxima varianza 
       * de la proyección vertical
       *****************************************/
      if (method == 'I')
	angulo=IDIAP_slant(inicio_tramo[tramo] ,fin_tramo[tramo]);
      else if(method == 'C')
	angulo=Javi_slant(inicio_tramo[tramo] ,fin_tramo[tramo]);
      else
	angulo=MVPV(inicio_tramo[tramo] ,fin_tramo[tramo]);

      shear(fixedImage, inicio_tramo[tramo],fin_tramo[tramo],(angulo)*M_PI/180.0);

      if (verbosity==1) 
	fprintf(stderr,"#slant: Tramo %d: Slant medio: %.0f\n",tramo,angulo+0.5);
    }
  }
  delete [] inicio_tramo;
  delete [] fin_tramo;

  return fixedImage;

}
void usage(char * progName,char * cad){
  fprintf(stderr,"Usage: %s %s\n",progName,cad);
  exit(1);
}
int main (int argc,char *argv[]) {
  
  string ifd, ofd;
  int verbosity=0;
  float threshold=1;
  int separacion_tramos=-1, longMinTramos=10;
  bool global=false;
  int option;
  char method='M';
  char usagecad[]=" [Options]\n \
          Options are:\n \
                 -i imput_image_file      by default stdin\n \
                 -o output_image_file     by default stdout\n \
                 -m method I|M|C          by defaul M(maxVar)\n \
                 -t threshold             by default 100%\n \
                 -s cols_between_words    by default authomatic\n \
                 -l longMinSegment        by default 10 cols\n \
                 -g                       slant global (by default local)\n \
                 -v level                 verbosity mode\n \
                 -h                       shows this information\n \
          Methods: I -> Idiap, M -> maxVar, C -> Contorno";


  while ((option=getopt(argc,argv,"hgt:v:i:o:m:s:l:"))!=-1)
    switch (option)
      {
      case 'i':
	ifd=optarg;
	break;
      case 'o':
	ofd=optarg;
	break;
      case 't':
	threshold=atof(optarg)/100.0;
	break;
      case 'v':
	verbosity=atoi(optarg);
	break;
      case 'm':
	method=optarg[0];
	if (method != 'M' && method != 'I' && method != 'C'){
	  usage(argv[0],usagecad);
	  exit(-1);
	}
	break;
      case 's':
	separacion_tramos=atoi(optarg);
	break;
      case 'l':
	longMinTramos = atoi(optarg);
         break;
      case 'g':
	global=true;
	break;
      case 'h':
	usage(argv[0],usagecad);
	break;
      default:
	usage(argv[0],usagecad);
      }
  

  slantClass frase(method,global,threshold,separacion_tramos,longMinTramos);

  if(frase.read(ifd)==-1)
    return -1;
  
  frase.ProuNegre=frase.otsu();
  frase.verbosity=verbosity;

  imageClass * dslant =  frase.deslant();

  dslant->write(ofd);
  
  delete dslant;
  return 0;
}
