/*
    Copyright (C) 1998 Moisés Pastor <mpastorg@dsic.upv.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "libpgm.h"
#define UMBRAL_VERTICAL 1
#define SEPARACION_MIN_LETRAS 25  /* en normtamanyo era 10 */

#define MIN(x,y) ( ((x)<(y)) ? (x) : (y) )
#define MAX(x,y) ( ((x)>(y)) ? (x) : (y) )

int * getV_Projection(gray **img,int rows,int cols, gray imaxval);
int tamanyo_medio_huecos(int * V_Projection, int cols);
int deteccionTramos(gray ** img, int ** inicio_tramo, int ** fin_tramo,int rows, int cols, gray imaxval);
void copiar(gray **origen, gray **destino,int cols, int rows) ;
void trasladarpgm(gray **origen, gray **destino, int col_inicial, int col_final, int incy, int rows);
gray ** centrarLinBase(gray ** img, int n_tramos, int *inicio, int *fin, int * cogxlw, int rows, int cols);
gray ** crop(gray ** img,int cols, int rows, int *rows_crop, int *cols_crop, gray imaxval);
gray ** row_crop(gray ** img,int cols, int rows, int *rows_crop, gray imaxval);
