/*
    Copyright (C) 2013 Moisés Pastor

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define UMBRAL_VERTICAL 1

#include <stdlib.h>
#include <math.h>
#include <cfloat>
#include <unistd.h>
#include <unistd.h>
#include <imageClass.h>

/*****************************************************************************/

#define MIN(x,y) ( ((x)<(y)) ? (x) : (y) )
#define MAX(x,y) ( ((x)>(y)) ? (x) : (y) )

/*****************************************************************************/

class slopeClass: public imageClass {
  int separacion_tramos, longMinTramo;

  int deteccionTramos( int ** inicio_tramo, int ** fin_tramo);
  imageClass * rotar(int n_tramos, int * inicio, int * fin, int * row_central, int * col_central,float * angle);
  void getContourn(imageClass & ubl,  imageClass & lbl, int n_tramos, int * inicio_tramo, int * fin_tramo);
  void ajuste_cont(imageClass & image,int col_ini,int col_fi, int *row_ini, int *row_fi);
  float line_detector(imageClass, int, int, int, int,int * massCenterX, int * massCenterY);
  imageClass * alinear(imageClass * origen, int n_tramos, int * col_inicial, int * col_final, int * massCenterX, int averageMassCenterX) ;
  int calculateMassCenterY(imageClass & cont, int col_ini, int col_fi, int row_ini, int row_fi);
public:
  float threshold;
  slopeClass(int separacion_tramos=-1, int longMinTramo=10) {
    this->separacion_tramos=separacion_tramos;
    this->longMinTramo = longMinTramo;
  }
  imageClass * deslope();

};

imageClass * slopeClass::rotar( int n_tramos, int * inicio, int * fin, int * row_central, int * col_central,float * angle) {
  float original_row, original_col;
  gray auxLL, auxUL, auxLU, auxUU;
  float auxL, auxU, aux;
  int row, col, rowL, rowU, colL, colU;
  double c_angle, s_angle;


  imageClass * rotada=new imageClass(rows,cols,maxval);

  for (int tramo=0; tramo<n_tramos; tramo++){
    if (verbosity){
      cerr << "\t# Slope: tramo "<< tramo << " ("<< inicio[tramo] << ","<<fin[tramo] << ")\t rotado\t " << angle[tramo] << endl;}

    if (angle[tramo] != 0){
      c_angle=cos(angle[tramo]);
      s_angle=sin(angle[tramo]);
      
      
      for (col=inicio[tramo]; col<fin[tramo]; col++)
    	for (row=0; row<rows; row++) {

    	  /* exact sampling location: */
    	  original_row =  c_angle*(row-row_central[tramo]) +		\
    	    s_angle*(col-col_central[tramo]) + row_central[tramo];
    	  original_col = -s_angle*(row-row_central[tramo]) +		\
    	    c_angle*(col-col_central[tramo]) + col_central[tramo];
	  
    	  /* four nearest coefficients to the sampling location: */
    	  rowL = ((int)original_row);   // round of int
    	  rowU = ((int)original_row)+1;
    	  colL = ((int)original_col);   // round of int
    	  colU = ((int)original_col)+1;

    	  /* Lagrangian interpolation: */
    	  auxLL = (colL>=inicio[tramo] && colL<fin[tramo] && rowL>=0 && rowL<rows)?image[rowL][colL]:maxval;
    	  auxUL = (colL>=inicio[tramo] && colL<fin[tramo] && rowU>=0 && rowU<rows)?image[rowU][colL]:maxval;
    	  auxLU = (colU>=inicio[tramo] && colU<fin[tramo] && rowL>=0 && rowL<rows)?image[rowL][colU]:maxval;
    	  auxUU = (colU>=inicio[tramo] && colU<fin[tramo] && rowU>=0 && rowU<rows)?image[rowU][colU]:maxval;

    	  auxL = (rowU-original_row)*auxLL*(PGM_MAXVAL/maxval) +	\
    	    (original_row-rowL)*auxUL*(PGM_MAXVAL/maxval);
    	  auxU = (rowU-original_row)*auxLU*(PGM_MAXVAL/maxval) +	\
    	    (original_row-rowL)*auxUU*(PGM_MAXVAL/maxval);
    	  aux = (original_col-colL)*auxU + (colU-original_col)*auxL;
	
    	  rotada->image[row][col]= (gray)(aux+.5);
    	}
    } else {
      //copiar tramo
      for (col=inicio[tramo]; col<fin[tramo]; col++)
    	for (row=0; row<rows; row++) 
    	  rotada->image[row][col]=image[row][col];
    }
  }
  
  return rotada;
}
imageClass *  slopeClass::alinear(imageClass * origen, int n_tramos, int * col_inicial, int * col_final, int * massCenterY, int averageMassCenterY) {
  
  imageClass * destino = new imageClass(rows, cols, maxval);

  for (int tramo=0; tramo < n_tramos; tramo++){
    
    int incy=massCenterY[tramo] - averageMassCenterY;

    for (int col=col_inicial[tramo]; col<col_final[tramo]; col++)
      if (incy<0)
	for (int row=0;row<rows;row++)
	  if (row+incy>=0)
	    destino->image[row][col]=origen->image[row+incy][col];
	  else 
	    destino->image[row][col]=maxval;
      else
	for (int row=rows-1;row>=0;row--)
	  if (row+incy<rows)
	    destino->image[row][col]=origen->image[row+incy][col];
	  else 
	    destino->image[row][col]=maxval;
  }
  return destino;
}


int slopeClass::deteccionTramos( int ** inicio_tramo, int ** fin_tramo){
    int  n_tramos=0, i,hueco;
    int *histver;
    
    *inicio_tramo=new int [cols];
    *fin_tramo= new int[cols];
    
    /* Calculamos la proyeccion vertical */
    histver=getVerticalProjection();
   
    /* Calculamos el tamaño medio de los huecos */
    if (separacion_tramos==-1){
      separacion_tramos=tamanyo_medio_huecos(histver, cols);
      separacion_tramos += separacion_tramos * 0.1;
    }
    /* Dividimos la imagen en tramos contiguos*/
    n_tramos=0;
   /* quitamos posibles blancos iniciales */
    i=0;
    while (i < cols && histver[i] < UMBRAL_VERTICAL)  i++;
    if (i==cols) 
        return (-1);

    /*Dividimos en tramos */
    (*inicio_tramo)[n_tramos]=i; 
    while(1) {
        for (; i<cols && histver[i]>=UMBRAL_VERTICAL; i++);
        if (i==cols) { 
            (*fin_tramo)[n_tramos]=cols-1; 
            break;
        }
        (*fin_tramo)[n_tramos]=i; 
        for (hueco=0; i<cols && histver[i]<UMBRAL_VERTICAL; i++, hueco++);
        if (i==cols) { 
            (*fin_tramo)[n_tramos]=cols-1; 
            break;
       }
        if (hueco>=separacion_tramos) {
            n_tramos++;
            (*inicio_tramo)[n_tramos]=i;
        }
    }
    delete [] histver;
    return(n_tramos+1);
    
}


void slopeClass::getContourn(imageClass & ubl,  imageClass & lbl, int n_tramos,int * inicio, int *fin) {
  //threshold=otsu();

  imageClass * RLSA = new imageClass(*this);
  int longMediaLetra=40;

  // background image to 0
  for (int f=0; f<rows; f++)
    for (int c = 0; c < cols; ++c){
      ubl.image[f][c]=0;
      lbl.image[f][c]=0;
    }

  // RLSA
  for (int tramo=0; tramo < n_tramos; tramo++){
    for (int row=0; row<rows; row++){
      int cont=0;
      int firstTime=1;
      for (int col=inicio[tramo]; col< fin[tramo];col++)
	if (image[row][col]<=threshold){
	  if(firstTime)firstTime=0;
	  else
	    if (cont < longMediaLetra)
	      for (int c2=col-cont; c2 <= col; c2++)
		RLSA->image[row][c2]=0;
	  cont=0;
	} else
	  cont++;
    }
  }

  /////////////////////////////
 for (int tramo=0; tramo < n_tramos; tramo++){
   for (int r=0; r<rows; r++){
     RLSA->image[r][inicio[tramo]]=0;
     RLSA->image[r][fin[tramo]]=0;
   }

 }
  for (int tramo=0; tramo < n_tramos; tramo++){
    /* obtencion de los bordes superior e inferior */ 
    int cont,col,row,max_cont,row_fi;
    for (col=inicio[tramo];col<fin[tramo];col++){
      cont=0;
      max_cont=0;
      row_fi=0; //
      for (row=0;row<rows;row++){
	if (RLSA->image[row][col]==0) 
	  cont++;
	else{
	  if (cont > max_cont){
	    max_cont=cont;
	    row_fi=row;
	  }
	  cont=0;
	}
      }
      if((row_fi == 0) && (cont > 0)){ 
	row_fi = rows-1; 
	max_cont=cont-1;
	 
      }

      ubl.image[row_fi-max_cont][col]=1; //as in binary images
      lbl.image[row_fi][col]=1;
    }
  }
  if (verbosity>=3){
    RLSA->write("RLSA.pgm");
    ubl.write("cont_uper_cont.pgm");
    lbl.write("cont_down.pgm");
  }
  
  delete RLSA;
}

int slopeClass::calculateMassCenterY(imageClass & cont, int col_ini, int col_fi, int row_ini, int row_fi){
  float acumX=0, acum=0;
  for (int r=row_ini; r<row_fi; r++){
    for (int c=col_ini; c<col_fi; c++){
      acum +=  (cont.image[r][c]); // cont as a binary image. 1 means foreground
      acumX += (cont.image[r][c]) * r;
    }
  }
  if (acum==0) return 0;
  return acumX/acum + 0.5;

}

void slopeClass::ajuste_cont(imageClass & contourn,int col_ini,int col_fi, int *row_ini, int *row_fi){
  int *hist;
  int row,col,i,totMasa,acumulado;

  hist=(int*)malloc(rows*sizeof(int));

  for(row=0;row<rows;row++)
    hist[row]=0;

  totMasa=0;
  for(row=0;row<rows;row++){
    for(col=col_ini;col<col_fi;col++)
      hist[row]+=contourn.image[row][col]; 
    totMasa+=hist[row];
  }
  totMasa/=2;

  for (i=0,acumulado=0; i<rows && acumulado<totMasa; i++)
    acumulado+=hist[i];

  *row_ini = i - (int)(1+(col_fi-col_ini)*.06);
  *row_fi  = i + (int)(1+(col_fi-col_ini)*.06);

  if (*row_ini<0) *row_ini=0;
  if (*row_fi> rows) *row_fi=rows-1;
}

float slopeClass::line_detector(imageClass contourn,int row_inicio, int col_inicio, int row_final, int col_final,int * massCenterX, int * massCenterY)
{
  int row,col;
  float sum,sumx,sumy,sumxx,sumxy,sumyy;
  float scatterxx,scatterxy,scatteryy;
  float lambda1,lambda2,lambda,aux,aux2;
  int pixel;

  sum=sumx=sumy=sumxx=sumxy=sumyy=0.0;
  for (row=row_inicio;row<row_final;row++)
    for (col=col_inicio;col<col_final;col++) {
      pixel=contourn.image[row][col];
      sum+=pixel;
      sumx+=pixel*row;
      sumy+=pixel*col;
      sumxx+=pixel*row*pixel*row;
      sumxy+=pixel*row*pixel*col;
      sumyy+=pixel*col*pixel*col;
    }
  /* scatter matrix of the standardized points:
     
     | scatterxx scatterxy |   | sum(x-mx)*(x-mx)  sum(x-mx)*(y-my) |
     |                     | = |                                    |
     | scatterxy scatteryy |   | sum(x-mx)*(y-my)  sum(y-my)*(y-my) |
   
                              | sumxx-sumx*sumx/n  sumxy-sumx*sumy/n |
                            = |                                      |
                              | sumxy-sumx*sumy/n  sumyy-sumy*sumy/n | */

  scatterxx=sumxx-sumx*sumx/sum;
  scatterxy=sumxy-sumx*sumy/sum;
  scatteryy=sumyy-sumy*sumy/sum;

  /* maximum eigenvalue:
     
    | scatterxx-lambda         scatterxy |
    |                                    | = 0
    | scaterxy          scatteryy-lambda |

    ==> lambda*lambda - (scatterxx+scatteryy) * lambda +
        (scatterxx*scatteryy - scatterxy*scatterxy) = 0 */

  aux=scatterxx+scatteryy;
  aux2=sqrt(aux*aux-4*(scatterxx*scatteryy-scatterxy*scatterxy));
  lambda1=(aux+aux2)/2.0;
  lambda2=(aux-aux2)/2.0;
  lambda=MAX(lambda1,lambda2);

  /* slope:

     (scatterxx-lambda)*x +          scatterxy*y = 0 |
                                                     | ==> (adding both eqs)
              scatterxy*x + (scatteryy-lambda)*y = 0 |

               lambda - scatterxx - scatterxy
     ==>  y = ------------------------------- * x
               scatteryy + scatterxy - lambda        */


  *massCenterX=sumx/sum;
  *massCenterY=sumy/sum;

  aux=(lambda-scatterxx-scatterxy)/(scatteryy+scatterxy-lambda);
  if (aux==aux) /* number, including -Inf and Inf */
    return MIN(MAX(aux,-FLT_MAX),FLT_MAX);
  else /* not a number (NAN) */
    return 0.0;

}

imageClass * slopeClass::deslope(){
  
  int *inicio_tramo,*fin_tramo, n_tramos, tramo;
  
  
  /***************************************
   * Deteccion de tramos 
   ***************************************/
  
  n_tramos=deteccionTramos(&inicio_tramo,&fin_tramo);
  
  if (verbosity>=1){
    cerr << "# Slope: " << n_tramos << " Tramos encontrados "<< endl;
    cerr << "# Slope: tamanyo medio hueco "<<separacion_tramos<< endl;
  }

  /* Deteccion de lineas base */
  imageClass ubl(rows,cols,1);
  imageClass lbl(rows,cols,1);

  getContourn(ubl,lbl, n_tramos, inicio_tramo, fin_tramo);
  
  float averageMassCenterY=0;
  int * massCenterX = new int [n_tramos];
  int * massCenterY = new int [n_tramos];
  int * lowMassCenterY = new int [n_tramos];
  float * angles = new float[n_tramos];
  
  for (tramo=0; tramo<n_tramos; tramo++){
    if (fin_tramo[tramo]-inicio_tramo[tramo] > longMinTramo) {
      
      /* Ajuste del tramo por eigenvector */
      int row_ini,row_fi,massCenterXUP, massCenterYUP,massCenterXDOWN, massCenterYDOWN ;
      ajuste_cont(ubl,inicio_tramo[tramo],fin_tramo[tramo], &row_ini, &row_fi);
      float slopeup= line_detector(ubl,row_ini,inicio_tramo[tramo],row_fi,fin_tramo[tramo], &massCenterYUP, &massCenterXUP);
      
      ajuste_cont(lbl,inicio_tramo[tramo],fin_tramo[tramo],&row_ini,&row_fi);
      float slopelw=line_detector(lbl,row_ini,inicio_tramo[tramo],row_fi,fin_tramo[tramo], &massCenterYDOWN, &massCenterXDOWN);
      
      angles[tramo]=(-atan(-1.0/slopeup)-atan(-1.0/slopelw))/2.0;

      massCenterX[tramo]=(massCenterXUP+massCenterXDOWN)/2;
      massCenterY[tramo]=(massCenterYUP+massCenterYDOWN)/2;
      lowMassCenterY[tramo] = massCenterYDOWN;
      averageMassCenterY+=lowMassCenterY[tramo];
      
    } else {
      if (verbosity)
	cerr << "\t# Slope: Warning Tramo " << tramo << " --> NO PROCESSED\n";

      int row_ini,row_fi;
      ajuste_cont(lbl,inicio_tramo[tramo],fin_tramo[tramo],&row_ini,&row_fi);
      massCenterY[tramo]=rows/2;
      massCenterX[tramo]=fin_tramo[tramo]-inicio_tramo[tramo]/2;
     
      angles[tramo]=0;
      averageMassCenterY+=massCenterY[tramo];
     
      lowMassCenterY[tramo]=  calculateMassCenterY(lbl, inicio_tramo[tramo], fin_tramo[tramo], row_ini, row_fi);
    }
  }
  imageClass * rotada = rotar(n_tramos, inicio_tramo, fin_tramo, massCenterY, massCenterX, angles);

  // Pinta la linea base de todos los tramos
  /* for (int t=0; t<n_tramos;t++)
    for (int c=inicio_tramo[t]; c< fin_tramo[t]; c++)
      rotada->image[lowMassCenterY[t]][c]=0;
  */
  averageMassCenterY /=n_tramos;

  return alinear(rotada, n_tramos, inicio_tramo, fin_tramo, lowMassCenterY, averageMassCenterY);
}


void usage(char * prog, char * cad){
  cerr << "Usage: " <<  prog << " " << cad << endl;
  exit(1);
}


int main (int argc, char *argv[]) {
  
  
  string ifd, ofd;
  int verbosity=0;
  int separacion_tramos=-1, longMinTramos=50;;
  int option;

  char usagecad[]=" [Options]\n \
          Options are:\n \
                 -i imput_image_file      by default stdin\n \
                 -o output_image_file     by default stdout\n \
                 -s #size between words   by default automatic\n \
                 -l longMinSegment        by default 50 cols\n \
                 -h                       shows this information\n \
                 -v level                 verbosity mode\n";

  while ((option=getopt(argc,argv,"hv:s:i:o:l:"))!=-1)
      switch (option) {
      case 'i':
        ifd=optarg;
        break;
      case 'o':
        ofd=optarg;
        break;
      case 's':
	separacion_tramos=atoi(optarg);
	break;
      case 'l':
	longMinTramos = atoi(optarg);
         break;
      case 'v':
	verbosity=atoi(optarg);
	break;
      case 'h':
	usage(argv[0],usagecad);
	break;
      default:
	usage(argv[0],usagecad);
}

  slopeClass frase(separacion_tramos,longMinTramos);


  if(frase.read(ifd)==-1)
    return -1;

  frase.threshold=frase.otsu();
  frase.verbosity=verbosity;

  imageClass * deslope =  frase.deslope();


  //imageClass * deslopeCrop = deslope->crop(true,false); //vertical crop
  //deslopeCrop->write(ofd);
  
  deslope->write(ofd);
  delete deslope;

  //delete deslopeCrop;
  return 0; 
}

