#!/bin/bash

# Copyright (C) 1997 by Pattern Recognition and Human Language
# Technology Group, Technological Institute of Computer Science,
# Valencia University of Technology, Valencia (Spain).
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted, provided
# that the above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation.  This software is provided "as is" without express or
# implied warranty.

set -e
export LC_NUMERIC=C

PATH=$PATH:scripts:/home/moises/bin_bak
NAME=${0##*/}
if [ $# -lt 7 -o $# -gt 9 ]; then
  echo "Usage: $NAME <train.lst> <HMMs-Dir> <samplesLabels.mlf> <HMMsList> <#Num-Iter> <#Num-Gauss> <#NumVec-size> [<HCompV-f-parameter>] [<HCompV-v-parameter>]"
  exit 1
fi

LSTTRA=$1
DIRHMM=$2
LABELS=$3
L_HMMS=$4
NUM_ITER=$5
NUM_GAUSS=$6
D=$7
fVar=0.001; if [ $# -gt 8 ]; then fVar=$8; fi
vVar=0.1; if [ $# -gt 9 ]; then vVar=$9; fi


#############################################
# Configuración de los parámetros de HTK
#############################################
FLAGSHEREST="-A -T 1 -m 3"
#############################################


#####################################################################
# 			    MAIN PROGRAM
#####################################################################

if [ -d $DIRHMM/hmm_0 ]; then
  g=1; while [ $g -le $NUM_GAUSS ]; do
    [ -d $DIRHMM/hmm_$g ] || break
    g=$[g*2]
  done
else g=0
fi

if [ $g -eq 0 ]; then 
  echo "Inicializacion..." 1>&2
  mkdir -p $DIRHMM/hmm_0 

  echo "Create_HMMs-TOPOLOGY.sh $D 1 proto >proto"
  Create_HMMs-TOPOLOGY.sh $D 1 proto >proto
  HCompV -A -T 1 -m -f ${fVar} -v ${vVar} \
         -S $LSTTRA -M $DIRHMM/hmm_0 proto 1>&2

  M=$(grep -m1 -A1 "<MEAN>" $DIRHMM/hmm_0/proto | tail -n 1)
  V=$(grep -m1 -A1 "<VARIANCE>" $DIRHMM/hmm_0/proto | tail -n 1)
  G=$(grep -m1 "<GCONST>" $DIRHMM/hmm_0/proto)

  head -3 $DIRHMM/hmm_0/proto > $DIRHMM/hmm_0/Macros_hmm
  cat $DIRHMM/hmm_0/vFloors >> $DIRHMM/hmm_0/Macros_hmm

  cat ${L_HMMS} |
  while read -a S; do
    Create_HMMs-TOPOLOGY.sh $D ${S[1]} ${S[0]} | tail -n +2
  done |
  sed -r "/<MEAN>/{n;s/^.*$/$M/}; \
          /<VARIANCE>/{n;s/^.*$/$V\n$G/}" >> $DIRHMM/hmm_0/Macros_hmm

  g=1
fi

S=1000
awk '{print $1}' ${L_HMMS} > AuxHMMsList
while [ $g -le ${NUM_GAUSS} ]; do
  mkdir $DIRHMM/hmm_$g
  echo "Creando hmm con $g gaussianas ..." 1>&2
  if [ $g -eq 1 ]; then
    cp $DIRHMM/hmm_$[g-1]/Macros_hmm $DIRHMM/hmm_$g/Macros_hmm
  else
    echo "MU $g {*.state[2-$[S-1]].mix}" > mult_gauss_$g
    HHEd -A -H $DIRHMM/hmm_$[g/2]/Macros_hmm -M $DIRHMM/hmm_$g\
      mult_gauss_$g AuxHMMsList
    rm mult_gauss_$g
  fi
  echo "Reestimacion de hmm_$g con $g gaussianas ..."  1>&2
  k=1
  while [ $k -le ${NUM_ITER} ]; do
    echo "Reestimacion $k de hmm_$g con $g gaussianas ..."  1>&2
    HERest $FLAGSHEREST -S $LSTTRA -I $LABELS -H $DIRHMM/hmm_$g/Macros_hmm AuxHMMsList
    k=$[k+1]
  done
  g=$[g*2]
done
