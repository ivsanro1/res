#!/bin/bash

# Copyright (C) 1997 by Pattern Recognition and Human Language
# Technology Group, Technological Institute of Computer Science,
# Valencia University of Technology, Valencia (Spain).
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted, provided
# that the above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation.  This software is provided "as is" without express or
# implied warranty.

########################################################################
# WARNING: This script has been updated to work with the Benthan dataset
########################################################################

set -e

NAME=${0##*/}
if [ $# -ne 3 ]; then
  echo "Usage: $NAME <GT-Transcripts-Dir> <#States> <output-File>" 1>&2
  exit 1
fi

DORG=$1
NS=$2
FDEST=$3
[ -d $DORG ] || { echo "$DORG doesn't exist ..."; exit 1; }

PUNTMRKS="-%,;:|)(\!\.'"

# NOTES:
# @: is used to represent the normal white space
# %: is used to represent the short white space,
#    between number and words y puntuations marks

{ echo "%"; cat $DORG/*.txt; } |
sed -r "s/^ //; s/ $//; s/[ ]+/@/g; s/./&\n/g" |
sort -u | sed "/^$/d" |
sed -r "s/[${PUNTMRKS}]$/&\t$[NS/2]/; s/^[^${PUNTMRKS}]$/&\t${NS}/" |
sed "s/\"/<dquote>/; s/'/<quote>/" > $FDEST

exit 0
