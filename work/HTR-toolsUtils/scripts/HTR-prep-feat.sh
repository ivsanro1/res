#!/bin/bash  
#export PATH=$PATH:./bin:.
export PATH=./bin:.:/usr/bin

NAME=${0##*/}
if [ $# -ne 2 ]; then
  echo "Usage: $NAME <Dir-source-pgm> <Dir-dest-pgm>" 1>&2
  exit
fi

DORG=$1
DDEST=$2
DIM=20 
V=2
O=2 
F=1

PTH=$(dirname $0)
[ -d $DORG ] || { echo "$DORG doesn't exist ..."; exit 1; }
[ -d $DDEST ] || mkdir $DDEST

for f in $DORG/*
do
  EXT=`echo $f|awk -F"." '{print $NF}'`
  name=`basename $f .$EXT` 
  
  echo "Processing $f"
  imgtxtenh -i $f -S 4 -a | imageSlope | imageSlant -m M -t 92 | pgmnormsize -c 5 |featExtraction -c $DIM -V $V -O $O -F $F -H -o $DDEST/${name}.fea

done
