#!/bin/bash

# Copyright (C) 1997 by Pattern Recognition and Human Language
# Technology Group, Technological Institute of Computer Science,
# Valencia University of Technology, Valencia (Spain).
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted, provided
# that the above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation.  This software is provided "as is" without express or
# implied warranty.

########################################################################
# WARNING: This script has been updated to work with the Benthan dataset
########################################################################

NAME=${0##*/}
if [ $# -ne 2 ]; then
  echo "Usage: $NAME <input-Dir> <output-File> " 1>&2
  exit 1
fi

DORG=$1
FDEST=$2

echo "#!MLF!#" > $FDEST

for f in $DORG/*.txt;
do
  F=$(basename $f .txt);
  cat $f |
  sed -r "\
      s/^ */@/; s/ *$/@/; s/[ ]+/@/g; \
      s/([-+:;,?\!\.0-9])([0-9])/\1%\2/g; \
      s/([-+:;,?\!\.0-9£])([0-9])/\1%\2/g; \
      s/@([-+:;,?\!\.=/\:\)]+)/%\1/g; \
      s/\(@/\(%/g; s/\[@/\[%/g; s/@\]/%\]/g" |
  awk -v fl="$F" \
       '{
          L=length($1);
	  print "\"*/"fl".lab\"";
	  for (l=1;l<=L;l++) {
	    C=substr($1,l,1);
	    if (C~/[0-9\.]/) print "\""C"\"";
	    else print C;
	  }
	  print "."
	}';
done |
sed "s/^\"$/<dquote>/; s/^'$/<quote>/" >> $FDEST
