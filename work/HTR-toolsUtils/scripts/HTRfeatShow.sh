#!/bin/bash
export LC_ALL=C

PROG=${0##*/}
if [ $# -ne 1 ]; then echo "Usage: $PROG <features-file>"; exit; fi
FEAFL=$1
[ -e $FEAFL ] || { echo "ERROR: $FEAFL does not exist !"; exit 1; }

TMP=./$$.aux
trap "rm $TMP* 2>/dev/null" EXIT

#HTK_PATH=~/bin_bak
#PATH=$PATH:$HTK_PATH

HList -r $FEAFL > $TMP
FOUT=$(basename $FEAFL)
maxval=255
Width=`awk 'END{print NR}' $TMP`
aux=`awk 'END{print NF}' $TMP`
Height=$((aux / 3))


#gray level
awk -v h=$Height -v w=$Width -v mx=$maxval '\
   BEGIN{ 
       #printf("P2\n%d %d\n%d\n",w,h,mx)
       printf("P2\n%d %d\n%d\n",w,3*h,mx)
       j=1
   }
   {
     for (i=1;i<=h;i++) {
        if ($i > 100) C[i,j]=100
          else C[i,j]=$i
 #      printf("%f ",$i);
     }

     C[h+1,j]=$((h*3)+2)
     C[h+2,j]=$((h*3)+4)
     j=j+1
   }

   END{
        for (i=1;i<=h;i++) {
            for (j=1;j<=w;j++) printf("%d ",int(mx * ((100-C[i,j])/100)));
            printf("\n")
        }
    }' $TMP > "${FOUT/.???/_fea.pgm}" 
    #}' $TMP > "${FOUT/.???/_gray.pgm}" 



#horizontal derivative
awk -v h=$Height -v w=$Width -v mx=$maxval -v pi=3.141516 '\
   BEGIN{
       #printf("P2\n%d %d\n%d\n",w,h,mx)
       j = 1
       Cmx = 0
   }
   {
       for (i=1;i<=h;i++) {
           C[i,j]=$(i+h)
	   ba=(C[i,j]<0)?-C[i,j]:C[i,j]
	   if (Cmx<ba) Cmx=ba
       }
       j = j + 1
   }
   END{
       for (i=1;i<=h;i++) {
          for (j=1;j<=w;j++) printf("%d ",int(mx/(2*Cmx)*C[i,j]+mx/2));
          printf("\n")
       }
   }' $TMP >> "${FOUT/.???/_fea.pgm}"
   #}' $TMP > "${FOUT/.???/_hdev.pgm}"

#vertical derivative
awk -v h=$Height -v w=$Width -v mx=$maxval -v pi=3.141516 '\
  BEGIN{
      #printf("P2\n%d %d\n%d\n",w,h,mx)
      j = 1
      Cmx = 0
  }
  {
      for (i=1;i<=h;i++) {
          C[i,j]=$(i+2*h);
	  ba=(C[i,j]<0)?-C[i,j]:C[i,j]
	  if (Cmx<ba) Cmx=ba
      }
      j = j + 1
  }
  END{
      for (i=1;i<=h;i++) { 
         for (j=1;j<=w;j++) printf("%d ",int(mx/(2*Cmx)*C[i,j]+mx/2));
         printf("\n")
      }
  }' $TMP >> "${FOUT/.???/_fea.pgm}"
  #}' $TMP > "${FOUT/.???/_vdev.pgm}"

