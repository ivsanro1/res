#!/bin/bash

# Copyright (C) 1997 by Pattern Recognition and Human Language
# Technology Group, Technological Institute of Computer Science,
# Valencia University of Technology, Valencia (Spain).
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted, provided
# that the above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation.  This software is provided "as is" without express or
# implied warranty.

########################################################################
# WARNING: This script has been updated to work with the Benthan dataset
########################################################################
PATH=$PATH:$HOME/bin_bak
set -e

NAME=${0##*/}
trap "rm -f tmp_sentences tmp.arpa 2>/dev/null" EXIT
#export LC_ALL=C

if [ $# -ne 4 ]; then
  echo "Usage: $NAME <trainList-File> <Transcripts-DIR> <output-FileDic> <output-FileNet>" 1>&2
  exit 1
fi

TFILE=$1
GTDIR=$2
oDIC=$3
oNET=$4
[ -e $iFILE ] || { echo "$iFILE doesn't exist ..."; exit 1; }


for f in $(< $TFILE); do cat $GTDIR/$f.txt; done |
tee tmp_sentences |
awk '{for (i=1;i<=NF;i++) print $i}' | sort -u |
awk '{
       L=length($1);
       printf "\""$1"\"\t1.0\t";
       if (substr($1,1,1)~/[-,\.,;:?\!|=\)]/) printf "%"; else printf "@";
       for (l=1;l<=L;l++) printf " "substr($1,l,1);
       print ""
     }' |
 sed -r \
  "s/([0-9£]) ([-,\.0-9])/\1 % \2/g;
    s/([0-9£]) ([-,\.0-9])/\1 % \2/g;
    s/([^-0-9£,\.,;:?\!|=\)%@]) ([-,\.,;:?\!|=\)])/\1 % \2/g;
    s/([^-0-9£,\.,;:?\!|=\)%@]) ([-,\.,;:?\!|=\)])/\1 % \2/g;
    s/@ \]/% \]/g; s/\"\"\"/\"\\\\\"\"/g; s/\"'\"/\"\\\'\"/g
    s/ \"/ <dquote>/g; s/ '/ <quote>/g" > $oDIC
#echo "@  []  @"  >> $oDIC    # For white space
echo "<s>  []"     >> $oDIC    # For initial symbol
echo "</s>  [] @"  >> $oDIC    # For ending symbol

sed -r "s/^/<s> /; s/$/ <\/s>/" tmp_sentences > train_sentences
ngram-count -text train_sentences -lm tmp.arpa -order 2 -ukndiscount1 -ukndiscount2
sed -r "s/['\"]/\\\&/g" tmp.arpa > LM.arpa
HBuild -n LM.arpa -s "<s>" "</s>" $oDIC $oNET
